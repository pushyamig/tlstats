#!/usr/bin/perl -w
# test that required perl modules are available.

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/moduleTest.t $
# $Id: moduleTest.t 1448 2012-02-13 02:15:34Z dlhaines $

use strict;
use Test::More qw(no_plan);

BEGIN {

      use_ok('Digest::MD5');
      use_ok('Time::HiRes');
      use_ok('LWP::UserAgent');
      use_ok('Sys::Syslog');
      use_ok('Socket');

      use_ok('BER');
      use_ok('RRDs');
      use_ok('SNMP_util');
      use_ok('SNMP_Session');

      use_ok('Rcs');

}

#end
