#!/bin/bash

# Run simple commands to check access to umiac servers

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/checkUmiac.sh $
# $Id: checkUmiac.sh 1834 2012-12-16 23:22:08Z dlhaines $

#set -x
# force error when see an unset variable.
set -u

TRACE=0;

TRIES=1;

## check simple command
#CMD="getSortName,dlhaines";
#REGEX="Haines,David";

## check most useful monitoring command.  This
## checks that the most used command is listed.
#CMD="getStats";
#REGEX="getSortName";

## Check umiac command that requires no data
## and so can check mininmal connectivity.
#    CMD="getDocs";
#    REGEX="Using the UMIAC API";




function checkBasicUmiac {
    HOST=$1;

    echo -n "################ check umiac access for [$HOST] ";

    if [ 0 -ne $TRACE ]; then
#	echo "command: wget --tries=2 --no-check-certificate http://${HOST}:8888/${CMD} -O - ";
	echo "command: wget --tries=$TRIES --no-check-certificate http://${HOST}:8888/${CMD} -O - ";
    fi;

    result=$(wget --tries=$TRIES --no-check-certificate http://${HOST}:8888/${CMD} -O - 2>&1 );

    if [[ "$result" =~ $REGEX ]] ; 
    then
	echo -n "++++++  umiac query worked";
    elif [[ "$result" =~ " 0K " ]];
	then
	    echo -n " +++++++ (no data) umiac query worked ";
    else
	if [[ "$result" =~ "No data received" ]];
	then
	    echo -n " no data received";
	fi;
	if [[ "$result" =~ "Connection refused" ]];
	then
	    echo -n "CONNECTION REFUSED ";
	fi;
	echo -n "  -------- UMIAC QUERY FAILED ";
	if [ 0 -ne $TRACE ]; then
	    #echo "command: [$command]";
#	    echo "command: wget --tries=2 --no-check-certificate http://${HOST}:8888/${CMD} -O - ";
	    echo "command: wget --tries=$TRIES --no-check-certificate http://${HOST}:8888/${CMD} -O - ";
	    echo "regex: [$REGEX]";
	    echo "result: [$result]";
	fi;
    fi
    echo " ";
}


## specify the test query / response to use.

TYPE="$@";

if [ "$TYPE" == "getSortName" ]; then
    CMD="getSortName,dlhaines";
    REGEX="Haines,David";
elif [ "$TYPE" == "getStats" ]; then
    CMD="getStats";
    REGEX="getSortName";
elif [ "$TYPE" == "getDocs" ]; then
    CMD="getDocs";
    REGEX="Using the UMIAC API";
else 
    echo "unrecognized option: [$TYPE]. Valid options are: getSortName getStats getDocs";
    CMD="getStats";
    REGEX="getSortName";
fi

echo "Using: cmd: [$CMD] regex: [$REGEX]";

#### It makes sense to check UMIAC connectivity for the instances
#### but remember that getStats will vary by server used, so that the 
#### getStats information from and instance dns name means nothing.

## CTIR ctqa
checkBasicUmiac umiacqa.dsc.umich.edu;

checkBasicUmiac bluebonnet.dsc.umich.edu;
checkBasicUmiac brilliant.dsc.umich.edu;

## CTIR ctdev
checkBasicUmiac umiacdev.dsc.umich.edu;

checkBasicUmiac ranchero.dsc.umich.edu;
checkBasicUmiac roadster.dsc.umich.edu;

## CTIR ctint
checkBasicUmiac umiacint.dsc.umich.edu;

checkBasicUmiac zephyr.dsc.umich.edu;
checkBasicUmiac zodiac.dsc.umich.edu;

## CTIR load
checkBasicUmiac umiacload.dsc.umich.edu;

checkBasicUmiac satellite.dsc.umich.edu;
checkBasicUmiac shadow.dsc.umich.edu;
checkBasicUmiac sierra.dsc.umich.edu;
checkBasicUmiac spirit.dsc.umich.edu;
checkBasicUmiac sprinter.dsc.umich.edu;
checkBasicUmiac stealth.dsc.umich.edu;

## CTIR prod
checkBasicUmiac umiac.dsc.umich.edu;

checkBasicUmiac caliber.dsc.umich.edu;
checkBasicUmiac caravan.dsc.umich.edu;
checkBasicUmiac cavalier.dsc.umich.edu;
checkBasicUmiac challenger.dsc.umich.edu;
checkBasicUmiac charger.dsc.umich.edu;
checkBasicUmiac colt.dsc.umich.edu;

#end
