#!/bin/bash

# Run simple commands to check access to umiac servers

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/loadUmiac.sh $
# $Id: loadUmiac.sh 1625 2012-04-16 19:42:05Z dlhaines $

#set -x
# force error when see an unset variable.
set -u

TRACE=0;

TRIES=1;

## check simple command
CMD="getSortName,dlhaines";
REGEX="Haines,David";

## check most useful monitoring command.  This
## checks that the most used command is listed.
#CMD="getStats";
#REGEX="getSortName";

## Check umiac command that requires no data
## and so can check mininmal connectivity.
#    CMD="getDocs";
#    REGEX="Using the UMIAC API";


function checkBasicUmiac {
    HOST=$1;

    echo -n "################ check umiac access for [$HOST] ";

    if [ 0 -ne $TRACE ]; then
#	echo "command: wget --tries=2 --no-check-certificate http://${HOST}:8888/${CMD} -O - ";
	echo "command: wget --tries=$TRIES --no-check-certificate http://${HOST}:8888/${CMD} -O - ";
    fi;

    result=$(wget --tries=$TRIES --no-check-certificate http://${HOST}:8888/${CMD} -O - 2>&1 );

    if [[ "$result" =~ $REGEX ]] ; 
    then
	echo -n "++++++  umiac query worked";
    elif [[ "$result" =~ " 0K " ]];
	then
	    echo -n " +++++++ (no data) umiac query worked ";
    else
	if [[ "$result" =~ "No data received" ]];
	then
	    echo -n " no data received";
	fi;
	if [[ "$result" =~ "Connection refused" ]];
	then
	    echo -n "CONNECTION REFUSED ";
	fi;
	echo -n "  -------- UMIAC QUERY FAILED ";
	if [ 0 -ne $TRACE ]; then
	    #echo "command: [$command]";
#	    echo "command: wget --tries=2 --no-check-certificate http://${HOST}:8888/${CMD} -O - ";
	    echo "command: wget --tries=$TRIES --no-check-certificate http://${HOST}:8888/${CMD} -O - ";
	    echo "regex: [$REGEX]";
	    echo "result: [$result]";
	fi;
    fi
    echo " ";
}


#     #!/bin/bash

# x=0		# Initialize x as zero
# while [ "$x" -lt "10" ]		# While x is less than ten, execute the do loop.
# do sleep 0.1				# Sleep slows things down a bit.
# 	let "x += 1"			# Increment x.
# 	echo -en "$x "			# Print the value of x without the newline character.
# done						# Complete the loop.
# echo						# Move the prompt down to a new line.
# exit 0

sleep=1.0;
function repeatCmd {
    local max=$1
    local host=$2;
    local x=0;
#    local max=10;
    echo "repeatCmd started at" $(date);
    while [ "$x" -lt "$max" ]
    do
	sleep $sleep;
	let "x += 1";
	echo -en ".$x";
#	echo "checkBasicUmiac $host";
	checkBasicUmiac $host;
    done
    echo "repeatCmd ended at" $(date);
}

# repeatCmd 20 umiacdev.dsc.umich.edu
# sleep 30;
# repeatCmd 20 ranchero.dsc.umich.edu
# sleep 30;
# repeatCmd 20 roadster.dsc.umich.edu

# repeatCmd 20 umiacint.dsc.umich.edu
# sleep 30;
# repeatCmd 20 zephyr.dsc.umich.edu
# sleep 30;
# repeatCmd 20 zodiac.dsc.umich.edu

# # ## CTIR ctqa
# repeatCmd 20 umiacqa.dsc.umich.edu; sleep 30;
# repeatCmd 20 bluebonnet.dsc.umich.edu; sleep 30;
# repeatCmd 20 brilliant.dsc.umich.edu; sleep 30;

# ## CTIR load
repeatCmd 20 umiacload.dsc.umich.edu; sleep 30;
repeatCmd 20 satellite.dsc.umich.edu; sleep 30;
repeatCmd 20 shadow.dsc.umich.edu; sleep 30;
repeatCmd 20 sierra.dsc.umich.edu; sleep 30;
repeatCmd 20 spirit.dsc.umich.edu; sleep 30;
repeatCmd 20 sprinter.dsc.umich.edu; sleep 30;
repeatCmd 20 stealth.dsc.umich.edu; sleep 30;

#### It makes sense to check UMIAC connectivity for the instances
#### but remember that getStats will vary by server used, so it 
#### so it means nothing.

# ## CTIR ctqa
# checkBasicUmiac umiacqa.dsc.umich.edu;
# checkBasicUmiac bluebonnet.dsc.umich.edu;
# checkBasicUmiac brilliant.dsc.umich.edu;

# ## CTIR ctdev
# checkBasicUmiac umiacdev.dsc.umich.edu;
# checkBasicUmiac ranchero.dsc.umich.edu;
# checkBasicUmiac roadster.dsc.umich.edu;

# ## CTIR ctint
# checkBasicUmiac umiacint.dsc.umich.edu;
# checkBasicUmiac zephyr.dsc.umich.edu;
# checkBasicUmiac zodiac.dsc.umich.edu;

# ## CTIR load
# checkBasicUmiac umiacload.dsc.umich.edu;
# checkBasicUmiac satellite.dsc.umich.edu;
# checkBasicUmiac shadow.dsc.umich.edu;
# checkBasicUmiac sierra.dsc.umich.edu;
# checkBasicUmiac spirit.dsc.umich.edu;
# checkBasicUmiac sprinter.dsc.umich.edu;
# checkBasicUmiac stealth.dsc.umich.edu;

# ## CTIR prod
# checkBasicUmiac umiac.dsc.umich.edu;
# checkBasicUmiac caliber.dsc.umich.edu;
# checkBasicUmiac caravan.dsc.umich.edu;
# checkBasicUmiac cavalier.dsc.umich.edu;
# checkBasicUmiac challenger.dsc.umich.edu;
# checkBasicUmiac charger.dsc.umich.edu;
# checkBasicUmiac colt.dsc.umich.edu;

#end
