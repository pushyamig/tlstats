#!/usr/bin/perl -w
#
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/tcMonitor.t $
# $Id: tcMonitor.t 1416 2012-02-06 01:18:11Z dlhaines $
#
# Copyright (C) 2000-2005 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve stats from a Tomcat server
# and stuff them into an RRD
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

use strict;
use Test::More qw(no_plan);

use Time::HiRes;
use LWP::UserAgent;

use FindBin;
my $homeDir = ${FindBin::Bin}."/../..";

require "$homeDir/bin/monitor.pl";

local $| = 1;

my($config);
$config->{'STEP'} = 300;
$config->{'DEBUG'} = 0;
$config->{'program'} = 'tcMonitor';
$config->{'version'} = "0.01";
#$config->{'homeDir'} = $ENV{'HOME'};
$config->{'homeDir'} = $homeDir;
#$config->{'dataDir'} = "/ctstats/data/tomcat";
$config->{'dataDir'} = $config->{'homeDir'}."/data/tomcat";
$config->{'timeout'} = 2;

my($tomcats);

my $monitorFile = "$homeDir/etc/tomcat.monitor";
my @urlLines = returnFileAsArray($monitorFile);

foreach (@urlLines) {
  chomp;
  next if (/(^#)|(^$)/);
  my($server, $notify, $user, $pass, $freeMemSite, $listURL) = split(/\|/);
  $tomcats->{$server}->{'user'} = $user;
  $tomcats->{$server}->{'pass'} = $pass;
  $tomcats->{$server}->{'webappCount'} = 0;
  $tomcats->{$server}->{'freeMemSite'} = $freeMemSite;
  $tomcats->{$server}->{'listURL'} = $listURL;
  $tomcats->{$server}->{'notify'} = $notify;
}

#printHash($tomcats,"tomcats: ");

# fail if there is nothing to do.
if (scalar(keys(%{$tomcats})) == 0) {
  fail("no test targets specified in monitor file.");
  exit 1;
}

for my $tc (sort(keys %{$tomcats})) {
  my($freeMemSite) = $tomcats->{$tc}->{'freeMemSite'};
  my($listSite) = $tomcats->{$tc}->{'listURL'};
  my($user) = $tomcats->{$tc}->{'user'};
  my($passwd) = $tomcats->{$tc}->{'pass'};
  my($timeout) = $config->{'timeout'}; #seconds
  my($notify) = $tomcats->{$tc}->{'notify'};

  # check the free / total memory page
  my($rval, $code, $status, $s, $tcp, $f, $z, $content) = check_httpd($freeMemSite, $user, $passwd, $timeout);
  is($code,200,"find requested page (memory) $freeMemSite");

  chomp($content);
  $content =~ s/\s+//g;
  my($freeMem,$totalMem) = split(/\|/, $content);
  like($freeMem,qr/\d+/i,"find free mem");
  like($totalMem,qr/\d+/i,"find total mem");
 
  # check on the page of running webapps
  my($nrval, $ncode, $nstatus, $ns, $ntcp, $nf, $nz, $ncontent) = check_httpd($listSite, $user, $passwd, $timeout);
  is($code,200,"find requested page (webapps) $listSite");

  my(@list) = split(/\n/, $ncontent);

  # make sure these some expected strings appear in the output
  my @result;

  @result = grep { $_ =~ m|/courier:running:\d+:courier|i } @list;
  ok(scalar(@result) > 0,"find courier");

  @result = grep { $_ =~ m|/sakai-site-tool:running:\d+:sakai-site-tool|i } @list;
  ok(scalar(@result) > 0,"find sakai-site-tool");

  @result = grep { $_ =~ m|/ctlib:running:\d+:ctlib|i } @list;
  ok(scalar(@result) > 0,"find ctools library");

}
