#!/bin/bash
# check that stats are updated on umiac server

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/testUmiacCluster.sh $
# $Id: testUmiacCluster.sh 2146 2015-06-04 16:50:55Z dlhaines $

# wget --tries=1 --no-check-certificate http://shadow.dsc.umich.edu:8888/getStats -O - 

#wget --tries=1 --no-check-certificate http://shadow.dsc.umich.edu:8888/getStats -O - 

CTDEV=( ranchero roadster umiacdev );
CTINT=( zephyr zodiac umiacint );
CTLOAD=( satellite shadow sierra spirit sprinter stealth umiacload );
CTOOLS=( caliber caravan cavalier challenger charger colt umiac );
CTQA=( bluebonnet brilliant umiacqa );

CMD="getSortName,dlhaines";

#HOSTS=${CTLOAD[@]};
#HOSTS=${CTQA[@]};
#HOSTS=${CTLOAD[@]};

H=$1;

case $H in 
    CTDEV )
	HOSTS=${CTDEV[@]};;
    CTINT )
	HOSTS=${CTINT[@]};;
    CTLOAD )
	HOSTS=${CTLOAD[@]};;
    CTOOLS )
	HOSTS=${CTOOLS[@]};;
    CTQA )
	HOSTS=${CTQA[@]};;
    * )
	echo "must specify instance name: CTDEV, CTINT, CTLOAD, CTOOLS, CTQA";
	exit 1;;
esac
	
echo "HOSTS: [$HOSTS]";

echo "started at " $(date)
for name in ${HOSTS[@]}; do
    echo "HOST: [$name]";
#    wget -q --tries=1 --no-check-certificate http://$name.dsc.umich.edu:8888/getSort -O - 
    wget -q --tries=1 --no-check-certificate http://$name.dsc.umich.edu:8888/$CMD -O - 
done
echo "end at " $(date)

#end
