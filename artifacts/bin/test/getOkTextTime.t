#!/usr/bin/perl
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/getOkTextTime.t $
# $Id: getOkTextTime.t 2146 2015-06-04 16:50:55Z dlhaines $

# Sanity test for the getOkTextTime.pl script

use strict;
use Test::More qw(no_plan);

use FindBin;
use lib "$FindBin::Bin";

BEGIN {
	require_ok('getOkTextTime.pl');
}

## this should fail
like(main("ctools.umich.ABBA"),qr/system:FAILED elapsed:\d/i,"result from bad host is as expected");

## this should work
like(main("ctools.umich.edu"),qr/system:ctools elapsed:\d+\.\d+/i,"result from cluster is as expected");

## this should work
like(main("tosca.dsc.umich.edu"),qr/system:ctools elapsed:\d+\.\d+/i,"result from single host is as expected");

#end
