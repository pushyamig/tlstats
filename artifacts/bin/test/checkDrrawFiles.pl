#!/usr/bin/perl
# Check settings on definition files.
# A) check that the RCS / definition files match up.
# B) check that the graph files have appropriate permissions and owners. (not implemented).
#
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/checkDrrawFiles.pl $
# $Id: checkDrrawFiles.pl 1590 2012-03-28 17:38:53Z dlhaines $
use strict;

my $SAVED = "/usr/local/ctools/app/ctools/ctstats/cgi-bin/saved";

my @FILES = `ls -l $SAVED `;
my @RCS_FILES = `ls -l $SAVED/RCS `;

my %FileName;
my %RCSFileName;
my %AllFiles;

#print "FILES: ",join("|",@FILES);
#print "RCS_FILES: ",join("|",@RCS_FILES);

foreach(@FILES) {
  /\d\s+(\w\d+\.\d+)$/;
  my $fileName = $1;
  $FileName{$fileName}++;
  $AllFiles{$fileName} += 1;
}

foreach(@RCS_FILES) {
  /\d\s+(\w\d+\.\d+),v$/;
  my $fileName = $1;
  $RCSFileName{$fileName}++;
  $AllFiles{$fileName} += 2;
}


## These definition files do not have a corresponding RCS file.

print "Files defined but without a backup\n";

foreach(sort(keys(%AllFiles))) {
    print "$_ : ",$AllFiles{$_},"\n" if ($AllFiles{$_} eq 1);
}

## These RCS files do not have a corresponding definition file.

print "Files with backups but not a separate defintion file\n";
foreach(sort(keys(%AllFiles))) {
    print "$_ : ",$AllFiles{$_},"\n" if ($AllFiles{$_} eq 2);
}

## These files have both definition and RCS files.
print "files that are defined and backed up.\n";
foreach(sort(keys(%AllFiles))) {
    print "$_ : ",$AllFiles{$_},"\n" if ($AllFiles{$_} eq 3);
}

# foreach(keys(%FileName)) {
# #  print "fk: $_\n";
#   unless($RCSFileName{$_}) {
#     print "$_ : no rcs file\n";
#   }
# }

# foreach(keys(%RCSFileName)) {
# #  print "fk: $_\n";
#   unless($FileName{$_}) {
#     print "$_ : only RCS file \n";
#   }
# }


#end
