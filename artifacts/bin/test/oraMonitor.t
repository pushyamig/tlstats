#!/usr/bin/perl -w
# test access to the ora database pages.
# 
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/oraMonitor.t $
# $Id: oraMonitor.t 1484 2012-02-20 19:06:19Z dlhaines $
#
# Copyright (C) 2000-2007 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve stats from a Oracle server (via a JSP)
# and stuff them into an RRD
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

use strict;
use Test::More qw(no_plan);
use FindBin;
my $homeDir = ${FindBin::Bin}."/../..";
require "$homeDir/bin/monitor.pl";

my $config = {};

$config->{'DEBUG'} = 0;
$config->{'program'} = 'oraMonitor';
$config->{'version'} = "0.01";
$config->{'homeDir'} = $homeDir;
$config->{'dataDir'} = $config->{'homeDir'}."/data/db";

#openlog($config->{'program'},'cons,pid', $config->{'logfacility'});

my $monitorFile = "$homeDir/etc/ora.test.monitor";

my @urlLines = returnFileAsArray($monitorFile);

my $instances = {};

foreach (@urlLines) {
  chomp;
  next if (/(^#)|(^$)/);
#  print "l: [$_]\n";
  my($server, $notify, $user, $pass, $statUrl, $dbname) = split(/\|/);
  $instances->{$dbname}->{'user'} = $user;
  $instances->{$dbname}->{'pass'} = $pass;
  $instances->{$dbname}->{'statUrl'} = $statUrl;
  $instances->{$dbname}->{'dbname'} = $dbname;
  $instances->{$dbname}->{'notify'} = $notify;
}

#printHash($instances,"ora instances");
# fail if there is nothing to do.
if (scalar(keys(%{$instances})) == 0) {
  fail("no test targets specified in monitor file.");
  exit 1;
}

for my $tc (sort keys %{$instances}) {
  my($statUrl) = $instances->{$tc}->{'statUrl'};
  my($dbname) = $instances->{$tc}->{'dbname'};
  my($user) = $instances->{$tc}->{'user'};
  my($passwd) = $instances->{$tc}->{'pass'};
  my($timeout) = 60;
  my($notify) = $instances->{$tc}->{'notify'};

  if ($user ne "" && $passwd ne "") {
  } else {
    fail("input line must have user and password");
  }
  my($nrval, $ncode, $nstatus, $ns, $ntcp, $nf, $nz, $ncontent) = check_httpd($statUrl, $user, $passwd, $timeout);
  #  $instances->{$tc}->{'collectiontime'} = time;

  is($ncode,200,"find requested page for $tc");

  my @result;
  my(@list) = split(/\n/, $ncontent);

  chomp(@list);

#  print "list: ",join("<<<\n",@list),"\n";


  if ($statUrl =~ /events.jsp/) {

    @result = grep { $_ =~ /sakai_realm/i } @list;
    ok(scalar(@result) > 0,"events.jsp: sakai_realm");

    @result = grep { $_ =~ /sakai_site\|myworkspace/i } @list;
    ok(scalar(@result) > 0,"events.jsp: sakai_site|myworkspace");

  }

  if ($statUrl =~ /dbstats.jsp/) {
    @result = grep { $_ =~ /Buffer Cache Hit Ratio/i } @list;
    ok(scalar(@result) > 0,"dbstats.jsp: Buffer Cache Hit Ratio");

    @result = grep { $_ =~ /tblsp.*CTOOLS_DATA/i } @list;
    ok(scalar(@result) > 0,"dbstats.jsp: tblsp.*CTOOLS_DATA");
  }

}
