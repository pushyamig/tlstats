#!/bin/bash
# Check for files not updated recently

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/checkRRDUpdates.sh $
# $Id: checkRRDUpdates.sh 1590 2012-03-28 17:38:53Z dlhaines $

DATA_DIR="../../data";
## +1 => not modified in at least 1 day.
## +7 => not modified in at least 1 week.
#AGE=" -mtime +7 "

## -1 modified in the last day.
#AGE=" -mtime -1 ";

## modified in the last hour.
AGE=" -mmin -60 ";

ACTION=" -ls ";

#set -x
find $DATA_DIR -name \*rrd $AGE $ACTION 

#end
