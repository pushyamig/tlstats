#!/bin/bash
# test existence of perl modules outside of perl

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/test/moduleTest.sh $
# $Id: moduleTest.sh 1448 2012-02-13 02:15:34Z dlhaines $

# This line lists the the location where the modules are found along
# with the module.

# perl -e 'while (<@INC>) { while (<$_/*.pm>) { print "$_\n"; } }'

# Check that this module can be found in perl by default.
function checkPerlModule() {
    local module=$1;
    local cmd="perl -M$module -e 'print \"module $module found\n\"'";
#    echo "cmd: [$cmd]";
    eval "$cmd";
}

# line below should fail
#perl -MDigest::WaitingForGodot -e 'print "module found\n"';

# These should succeed in normal install
checkPerlModule Digest::MD5;
checkPerlModule Time::HiRes;
checkPerlModule LWP::UserAgent;
checkPerlModule Sys::Syslog;
checkPerlModule Socket;

# these are required for ctstats.
checkPerlModule BER;
checkPerlModule RRDs;
checkPerlModule SNMP_Session;
checkPerlModule SNMP_util;

# this is good for drraw
checkPerlModule Rcs;

#end
