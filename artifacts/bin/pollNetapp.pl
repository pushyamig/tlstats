#!/usr/local/bin/perl -w
#
# Copyright (C) 2000-2006 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve stats from a netapp
# and stuff them into an RRD
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

use strict;
local $| = 1;

require '/afs/umich.edu/group/ct/ctlogs/ctools.umich.edu/bin/monitor.pl';

my($config);
$config->{'program'} = "pollNetapp";
$config->{'version'} = "0.01";
$config->{'homeDir'} = $ENV{'HOME'};
$config->{'libDir'} = $config->{'homeDir'} . "/ctools.umich.edu/lib";
$config->{'mibDir'} = $config->{'libDir'} . "/mibs";
$config->{'dataDir'} = $config->{'homeDir'} . "/ctools.umich.edu/data/netapp";
if (! defined $config->{'logfacility'}){
  $config->{'logfacility'} = 'user';
}
$config->{'STEP'} = 300;
$config->{'DEBUG'} = 0;
$config->{'timeout'} = 10;

openlog($config->{'program'},'cons,pid', $config->{'logfacility'});

use lib qw($config->{'libDir'});

use BER;
$BER::pretty_print_timeticks = 0;
use SNMP_util;
$SNMP_Session::suppress_warnings = 2;
snmpLoad_OID_Cache($SNMP_util::CacheFile);

opendir(DIR, $config->{'mibDir'}) || die "can't opendir $config->{'mibDir'}: $!";
my(@files) =readdir(DIR);
closedir DIR;

for my $mib (@files){
    chomp($mib);
    $mib = $config->{'mibDir'} . "/" . $mib;
    next if ($mib =~ /^\./); #skip dot files
    next unless ($mib =~ /\.mib/); #skip any files that don't have mib in their name
    my($rv) = &snmpQueue_MIB_File($mib);
#   my($rv) = &snmpMIB_to_OID($mib);
    if (! $rv && $config->{'DEBUG'}){
	print STDERR "* ERROR: Could not load $mib: $rv\n";
    }
}

my(%topLevel) = (
                 'miscGlobalStatus' => {
		     'value' => 3,
		     'message' => 'miscGlobalStatusMessage.0'
		     },
                 'envFailedFanCount' => {
                        'value' => 0,
                        'message' => 'envFailedFanMessage.0',
                        },
                 'envFailedPowerSupplyCount' => {
                        'value' => 0,
                        'message' => 'envFailedPowerSupplyMessage.0',
                        },
                 'fsOverallStatus' => {
                        'value' => 1,
                        'message' => 'fsStatusMessage.0',
                        },
                'diskFailedCount' => {
                        'value' => 0,
                        'message' => 'diskFailedMessage.0',
                        },
);
my(@tloids) = keys %topLevel;

my(%levelChecks);

$levelChecks{'default'}{'spareNumber'} = 0;
$levelChecks{'default'}{'nfsIsLicensed'} = 2;
$levelChecks{'default'}{'envOverTemperature'} = 1;
#$levelChecks{'nvramBatteryStatus'} = 9; # 'fully charged'
$levelChecks{'default'}{'nvramBatteryStatus'} = 1; # 'ok'
$levelChecks{'default'}{'cpuCount'} = 2;
$levelChecks{'default'}{'miscCorrectedMachineChecks'} = 0;
$levelChecks{'default'}{'ndmpOn'} = 1;
$levelChecks{'default'}{'diskReconstructingCount'} = 0;
$levelChecks{'default'}{'diskReconstructingParityCount'} = 0;
$levelChecks{'default'}{'diskVerifyingParityCount'} = 0;
$levelChecks{'default'}{'diskScrubbingCount'} = 0;
$levelChecks{'default'}{'diskAddingSpareCount'} = 0;
$levelChecks{'default'}{'snapmirrorOn'} = 2;
## deprecated?
##  'cifsIsEnabled'} = 1;
##  'ncIsEnabled'} = 1;
##  'quotaState'} = 2;
##  'quotaInitPercent'} = 0;

my($targets);
while(<>){
    next if (/(^$)|(^#)/);
    chomp;
    my($netapp, $rocommunity, $spares, $smenabled) = split(/\|/);
    $targets->{$netapp}->{'rocommunity'} = $rocommunity;
    $levelChecks{$netapp}{'spareNumber'} = $spares;
    $levelChecks{$netapp}{'snapmirrorOn'} = $smenabled;
}

for my $device (keys %{$targets}){
    $config->{'device'} = $device;
    my($agent) = "$targets->{$device}->{'rocommunity'}\@$config->{'device'}";

    my(@results) = ();
    for my $oid (keys %{$levelChecks{'default'}}){
	if (! defined $levelChecks{$device}{$oid}){
	    $levelChecks{$device}{$oid} = $levelChecks{'default'}{$oid};
	}
    }

    my(@lcoids) = keys %{$levelChecks{$device}};
	
    for my $set (\@tloids, \@lcoids){
	my($targetoids) = join(', ', @{$set});
	print STDERR $agent . " " . $targetoids . "\n";

	sub toresults {
	    my(@vals) = @_;
	    for (my $i=0;$i <= $#vals; $i++){
		if (! $vals[$i] || $vals[$i] eq ''){
		    $vals[$i] = 0;
		}
		print STDERR $vals[$i] . "|";
	    }
	    print STDERR "\n";
	    shift(@vals);
	for (my $i=0; $i<=$#@{$set}; $i++){
	    notify('debug', "$tloids[$i] -> $topLevel{$tloids[$i]}{'value'} got $results[$i]");

	}

	my($nrows) = &snmpmaptable($agent, \&toresults, $set);
	if ($nrows != 1){
	    notify('crit', "($config->{'device'}) snmpget failed of (${targetoids}) nrows ${nrows}");
	}
    }


#    my($nrows) = &snmpmaptable($agent, \&toresults, @tloids);
#    if ($nrows == 0 || ! defined $results[0]){
#	my($targetoids) = join(', ', @tloids);
#	my($vals) = join('|', @results);
#	notify('crit', "($config->{'device'}) snmpget failed of (${targetoids}) nrows ${nrows} with $vals");
#    } else {
#	for (my $i=0; $i<=$#tloids; $i++){
#	    notify('debug', "$tloids[$i] -> $topLevel{$tloids[$i]}{'value'} got $results[$i]");
#	    print STDERR $lcoids[$i] . "@" . $device . " -> " . $results[$i] . " ? " . $levelChecks{$device}{$lcoids[$i]} . "\n";
	    if ($results[$i] != $topLevel{$tloids[$i]}{'value'}){
		my(@errMsg) = snmpget(${agent}, $topLevel{$tloids[$i]}{'message'});
	        if (defined $errMsg[0]){
		    notify('crit', "($config->{'device'}) $errMsg[0]");
    	        } else {
		    notify('crit', "($config->{'device'}) snmpget of errMsg for $tloids[$i] == $results[$i]");
		}
	    }
	}
    }

    @results = ();
    print STDERR $agent . " " . join('|', @lcoids) . "\n";
    ($nrows) = &snmpmaptable($agent, \&toresults, @lcoids);
    if ($nrows == 0 || ! defined $results[0]){
	my($targetoids) = join(', ', @lcoids);
	my($vals) = join('|', @results);
	notify('crit', "($config->{'device'}) snmpget failed of (${targetoids}) nrows ${nrows} with $vals");
    } else {
	for (my $i=0; $i<=$#lcoids; $i++){
	    notify('debug', "$lcoids[$i] -> $levelChecks{$device}{$lcoids[$i]} got $results[$i]");
	    if ($results[$i] != $levelChecks{$device}{$lcoids[$i]}){
		notify('crit', "($config->{'device'}) $lcoids[$i] == |$results[$i]| (should be $levelChecks{$device}{$lcoids[$i]})");
	    } else {
	    }
	}
    }


closelog();


