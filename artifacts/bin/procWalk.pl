#!/usr/local/bin/perl -w

my($HOME) = $ENV{'HOME'};
my($LIB) = $HOME . "/ctools.umich.edu/lib";
use lib qw($LIB);

use strict;
local $| = 1;
my($DEBUG) = 0;

sub usage () {
    die "Usage: $0 host community\n";
}

require $ENV{'HOME'} . "/ctools.umich.edu/bin/snmpLib.pl";

use SNMP_util;
$BER::pretty_print_timeticks = 0;

$SNMP_Session::suppress_warnings = 1;
snmpLoad_OID_Cache($SNMP_util::CacheFile);

my($MIBdir) = $LIB . "/mibs";

opendir(DIR, $MIBdir) || die "can't opendir $MIBdir: $!";
my(@files) =readdir(DIR);
closedir DIR;

for my $mib (@files){
    chomp($mib);
    $mib = $MIBdir . "/" . $mib;
    next if ($mib =~ /^\./); #skip dot files
    next unless ($mib =~ /\.mib/); #skip any files that don't have mib in their name
    my($rv) = &snmpQueue_MIB_File($mib);
#  my($rv) = &snmpMIB_to_OID($mib);
    if (! $rv && $DEBUG){
        print STDERR "* ERROR: Could not load $mib: $rv\n";
    }
}

my($host) = $ARGV[0];
my($community) = $ARGV[1] || "public";
my($agent) = "${community}\@${host}";

my(@table) = (
#       "hrSWRunName",
        "hrSWRunPath",
        "hrSWRunParameters",
#       "hrSWRunType",
#       "hrSWRunStatus",
        "hrSWRunPerfCPU",
        "hrSWRunPerfMem",
                );

my($nrows) = &snmpmaptable($agent, \&printfun, @table);

