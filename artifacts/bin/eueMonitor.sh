#!/bin/bash
# wrapper to set environment variables before invoking perl script
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/eueMonitor.sh $
# $Id: eueMonitor.sh 1907 2013-03-20 16:08:00Z dlhaines $

#set -x

PERL=$(which perl)

# Find directory that the script is in (which may not be the current working directory).
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# get the environment if it exists
if [ -e $DIR/eueMonitor.env ]; then 
    ENV=$DIR/eueMonitor.env
    source $ENV
fi

$PERL $DIR/eueMonitor.pl "$@"

RC=$?
exit $RC

#end
