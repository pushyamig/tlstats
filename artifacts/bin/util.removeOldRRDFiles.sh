#!/bin/bash
# Remove unneeded database rrd files
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/util.removeOldRRDFiles.sh $
# $Id: util.removeOldRRDFiles.sh 1904 2013-03-20 15:48:48Z dlhaines $

set -x

# only work on the database files
# Don't change this to the top level directory or you will delete the legacy rrd files!!!!!
BASE=/usr/local/ctools/app/ctools/data/db

CMD=" -ls "
#CMD=" -delete "

echo "cleanup of: $BASE"

echo "space before cleanup"
df -h $BASE

echo "number of rrd files before cleanup"
find $BASE -name \*rrd | wc

# deal with the useless MISSING files
find $BASE -name \*MISSING\*\.rrd $CMD

# deal with db files that haven't changed in 30 days
find $BASE -name \*rrd -mtime +30 $CMD

echo "space after cleanup"
df -h $BASE

echo "number of rrd files after cleanup"
find $BASE -name \*rrd | wc

#end

