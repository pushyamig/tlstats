### STATUS
### 2013/03/07: trying version that can read name of script to run r1891 should be the 
###             production version before this change.

WebDriver - Ruby Notes:

Need to install ruby and the webdriver 'gem'
	- sudo apt-get install ruby1.9.1-full
	- sudo gem install selenium-webdriver

Download the latest selenium server from here:
	- http://seleniumhq.org/download/
	
To run script:
	Need to launch the webdriver server
		- java -jar selenium-server-standalone-2.28.0.jar

	From a separate command-line: ruby testHarness.rb

12/21:
	- To run testRunner standalone from command-line:
		- ruby -r "./testRunner.rb" -e "EUERunner.runTest 'tahoe'"
	- smile!

---- 
osx:
Installed ruby / webdriver on mac using ports
install json_pure via sudo gem install json_pure
also needed to install ffi

can get server via wget E.g. 
wget http://selenium.googlecode.com/files/selenium-server-standalone-2.28.0.jar
on mac needed to rename the /usr/bin ruby and gem to hide them and then link to the ones under /opt/local/bin
needed to install rb-json_pure, which installed ruby 1.8.7 ??
