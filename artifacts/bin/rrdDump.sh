#!/bin/bash
# Take list of rrd files and create xml dump files.
### TTD 
## make output directory a parameter.
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/rrdDump.sh $
# $Id: rrdDump.sh 1564 2012-03-15 18:27:52Z dlhaines $

set -u 
set -e

# Get a file names from command line 
# create dump file

for file in "$@"
do 
#    file=${file%.rrd}
    echo "dumping file: [$file]";
    rrdtool dump $file >|$file.xml;
done

#end
