#!/bin/bash
# Update the svn directories for the ctstats servers
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/updateCTStatsFromSvn.sh $
# $Id: updateCTStatsFromSvn.sh 1692 2012-05-15 18:22:20Z dlhaines $

BASE=/usr/local/ctools/app/ctools;
#BASE=/Users/dlhaines/tmp/testSvn;

DRY_RUN=0;
STATUS=0;

set -e
set -u
#set -x

function updateDirFromSvn {

    echo "******** updating directory: $1";

    (
	cd $1;

	echo ">>>>>>> before update"; 
	svn info;

	if [ 1 -eq $DRY_RUN ] ; then 
	    echo  "(dry run)";
	elif [ 1 -eq $STATUS ] ; then
	    svn status -u ;
	else
	    svn update .;
	fi

	echo "####### after update"; 
	svn info;
    )
}

updateDirFromSvn $BASE/bin;
updateDirFromSvn $BASE/cron;
updateDirFromSvn $BASE/lib;
updateDirFromSvn $BASE/www/cgi-bin;

#end
