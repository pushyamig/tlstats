#!/usr/bin/perl -w

use strict;
local $| = 1;

use RRDs;

my($DEBUG) = 0;

#printrrdinfo(@ARGV);
rraList(@ARGV);

sub printrrdinfo {
    my ($RRD) = @_;
    my $hash = RRDs::info $RRD;
    foreach my $key (keys %$hash){
	print "$key = $$hash{$key}\n";
    }
}

sub rraList {
    my($RRD) = @_;
    if ($DEBUG){ print STDERR "checking ${RRD}\n"; } 
    my($info) = RRDs::info $RRD;
    my(%rrarows);
    my(%rrapdps);
    for my $key (keys %{$info}){
	if ($key =~ /^rra\[(\d+)\]\.(pdp_per_)?rows?/){
	    if ($DEBUG){ print "${key} $1 = $$info{$key}\n"; }
	    if ($2){
		$rrapdps{$1} = $$info{$key};
	    } else {
		$rrarows{$1} = $$info{$key};
	    }
	}
    }
    for my $r (keys %rrapdps){
	if ($rrapdps{$r} == 1 && $rrarows{$r} < 43200){
	    if ($DEBUG){ print STDERR ${r} . " pdp: " . $rrapdps{$r} . " rows: " . $rrarows{$r} . "\n"; }
	    my($grow) = $rrarows{$r} + 43200;
	    $RRD =~ s/\$/\\\$/g;
	    $RRD =~ s/\'/\\\'/g;
	    $RRD =~ s/\ /\\ /g;
	    $RRD =~ s/\*/\\\*/g;
	    print <<LLI;
rrdtool resize ${RRD} $r GROW ${grow}; /bin/mv -f ${RRD} ${RRD}.old; /bin/chmod 644 ./resize.rrd; /bin/mv -f ./resize.rrd ${RRD};
LLI
        }
    }
}
