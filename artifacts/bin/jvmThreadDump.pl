#!/usr/local/bin/perl -w

my($LIB) = $ENV{'HOME'} . "/lib";
use lib qw($LIB);

use strict;
local $| = 1;
my($DEBUG) = 0;
my($dump) = 0;

sub usage () {
    die "Usage: $0 host community\n";
}

use SNMP_util;
$SNMP_Session::suppress_warnings = 1;
snmpLoad_OID_Cache($SNMP_util::CacheFile);

my($MIBdir) = $LIB . "/mibs";

opendir(DIR, $MIBdir) || die "can't opendir $MIBdir: $!";
my(@files) =readdir(DIR);
closedir DIR;

for my $mib (@files){
    chomp($mib);
    $mib = $MIBdir . "/" . $mib;
    next if ($mib =~ /^\./); #skip dot files
    next unless ($mib =~ /\.mib/); #skip any files that don't have mib in their name
    my($rv) = &snmpQueue_MIB_File($mib);
    if (! $rv && $DEBUG){
	print STDERR "* ERROR: Could not load $mib: $rv\n";
    }
}

my($host) = @ARGV;
chomp($host);
my($agent) = "public\@${host}:161:2.0:5:1.0:2";
#my($agent) = "public\@${host}";

sub printfun {
    #shift(@_);
    my(@vals) = @_;
    for (my $i=0;$i <= $#vals; $i++){
	if (! $vals[$i] || $vals[$i] eq ''){
	    $vals[$i] = 0;
	}
    }
    #next if (! @vals);
    #next if (! $vals[0] || ! $vals[1]);
    print join('|', @vals) . "|\n";
}

my($threadStats);
sub hashfun {
    my(@vals) = @_;
    my($i) = shift(@vals);
    my($name, $cpu, $waitt, $waits, $blockt, $blocks, $lockn, $lockown) = @vals;
    $threadStats->{$name}->{'count'}++;
    $threadStats->{$name}->{'cpu'} += $cpu;
    $threadStats->{$name}->{'waitt'} += $waitt;
    $threadStats->{$name}->{'waits'} += $waits;
    $threadStats->{$name}->{'blockt'} += $blockt;
    $threadStats->{$name}->{'blocks'} += $blocks;
#    $threadStats->{$name}->{'lockn'} += $lockn;
    $threadStats->{$name}->{'lockown'} += $lockown;
}

my(@cols) = (
#	     "jvmThreadInstIndex",
	     "jvmThreadInstName",
#	     "jvmThreadInstState",
	     "jvmThreadInstCpuTimeNs",
	     "jvmThreadInstWaitTimeMs",
	     "jvmThreadInstWaitCount",
	     "jvmThreadInstBlockTimeMs",
	     "jvmThreadInstBlockCount",
	     "jvmThreadInstLockName",
	     "jvmThreadInstLockOwnerPtr",
 	     );


my($nrows);
#my($nrows) = &snmpmaptable($agent, \&printfun, @cols);
if ($dump){
    ($nrows) = &snmpmaptable($agent, \&printfun, { 'use_getbulk' => 0 }, @cols);
    print $nrows;
} else {
    ($nrows) = &snmpmaptable($agent, \&hashfun, { 'use_getbulk' => 0 }, @cols);
    for my $t (keys %{$threadStats}){
	print $t . "|";
	for my $v (keys %{$threadStats->{$t}}){
	    print $threadStats->{$t}->{$v} . "|";
	}
	print "\n";
    }
}

