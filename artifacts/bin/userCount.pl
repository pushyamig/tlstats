#!/usr/bin/perl -w
#
# Copyright (C) 2000-2007 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve the number of users and event aggregates 
# using ctools servers and stuff them into RRD files.
# Uses call to jsp running on ctstats tomcat (e.g. feet).
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

use strict;
use FindBin;

my $homeDir = ${FindBin::Bin}."/..";

require "$homeDir/bin/monitor.pl";

my $config = {};
$config->{'DEBUG'} = 0;
$config->{'STEP'} = 300;

my($level) = "err";
if ($config->{'DEBUG'}) {
  $level = "crit";
}
$config->{'program'} = 'userCount';
$config->{'version'} = "0.01";

#print "**** HACKING DATA DIR in $0 ****\n";
#$config->{'dataDir'} = $homeDir . "/data";
#$config->{'dataDir'} = "/usr/local/ctools/tmp/data";

our $sharedDataDir;

$config->{'dataDir'} = $sharedDataDir;

openlog($config->{'program'},'cons,pid', $config->{'logfacility'});

my(%targets);
while (<>) {
  chomp;
  next if (/(^#)|(^\s*$)/);
  my($url, $cluster) = split(/\|/);
  $targets{$url} = $cluster;
}

#my($notify) = "ctstats-dev\@umich.edu";
print "**** HACKING NOTIFY in $0 ****\n";
my($notify) = "dlhaines\@umich.edu";
my($http_timeout) = 240;
for my $url (sort keys %targets) {
  my($rval, $code, $status, $s, $tcp, $f, $z, $content, $md5) = check_httpd($url,"","",$http_timeout);

  if ($config->{'DEBUG'}) {
    print <<CONTENT;
	-- CONTENT --
	    $url -> $rval|$code|$status|\n$content
  -- END CONTENT --
CONTENT
  }

  #only bother if we get real results
  if (defined($rval) && $rval > 0 && $code == 200) {
    #    chomp($content);
    #    $content =~ s/\s+//g;
    $content =~ s/\<body\>/\n/g;
    $content =~ s/\<br\>/|\n/g;
    my(@list) = split(/\n/, $content);
    my($i) = 1;
    my($collectionTime) = int($f);

    my(%afficounts, %afficountsnobits);

    for my $line (@list) {
      chomp($line);
      next if ($line =~ /this\ script\ runtime/);
      next unless ($line =~ /\w+\|\d*\|\d*$/);
      if ($config->{'DEBUG'}) {
	print "${i}: $line" . "\n";
	$i++;
      }
      my($type, $id, $id2, $val1, $val2) = split(/\|/, $line);

      my($RRD); 
      if ($type eq "users") {
	$RRD = "$config->{'dataDir'}/users/${id}.rrd";
      } elsif ($type eq "sessionlen") {
	$RRD = "$config->{'dataDir'}/sakai/session/${id}.rrd";
      } elsif ($type eq "event") {
	if ((! defined($id)) || ($id eq '')) {
	  $id = "null.event";
	}
	$RRD = "$config->{'dataDir'}/event/" . $targets{$url} . "/${id}.rrd";
      } elsif ($type eq "sakai_realm" || $type eq "sakai_site" || $type eq "sakai_site_tool") {
	$RRD = "$config->{'dataDir'}/sakai/" . $targets{$url} . "/${type}-${id}.rrd";
      } elsif ($type eq "affiliate_usage" || $type eq "dept_usage") {
	my($affi, $event) = ($id, $id2);
	$affi =~ s/^\_+//g;
	$affi =~ s/\.\.\./\-/g;
	$affi =~ s/\_+$//g;
	$affi =~ s/\-+$//g;
	$affi =~ s/\&/and/g;
	$affi =~ s/\(//g;
	$affi =~ s/\)//g;
	$affi =~ s/\//\-/g;
	$affi =~ s/\,//g;
	$affi =~ s/\./\-/g;
	$affi =~ s/\'//g;
	$affi =~ s/\"//g;
	$affi =~ s/^\s+//g;
	$affi =~ s/\s+$//g;
	$affi =~ s/\s+/_/g;

	my($subtype) = $type;
	$subtype =~ s/\_usage//g;

	my($rrdir) = "$config->{'dataDir'}/" . ${subtype} . "/" . $targets{$url} . "/${affi}";
	#print "making rrdir: [$rrdir]\n";
	# if (! ensureRRDDirectory($rrdir)) {
	#         notify('crit', "Could not create ${rrdir} ($!), skipping");
	#         next;
	# }

	$RRD = "${rrdir}/${event}.rrd";
	    
	$afficounts{$subtype}{$affi} += $val1;
	if ($event !~ (/^(pres.begin|pres.end|user.login|user.logout)$/)) {
	  $afficountsnobits{$subtype}{$affi} += $val1;
	}
      } else {
	notify($level, "unknown type ${type}, skipping");
	next;
      }

      if (! -e $RRD) {
	my($START) = time - $config->{'STEP'};

	notify('info', "Creating $RRD with step $config->{'STEP'} starting at $START");


	my($v, $msg) = RRD_create($RRD, $START, $config->{'STEP'}, $type);
	if ($v) {
	  notify('err', "couldn't create $RRD because $msg");
	  next;
	}
      }

      my(@vals) = ($val1, $val2);
      my($rv, $errmsg) = updateRRD($RRD, $collectionTime, @vals);
      if ($rv) {
	notify($level, "error updating $RRD : ${errmsg}");
      }
    }
    if ($targets{$url} eq "CTOOLS") {
      #provide total event counts for each affiliate/dept
      for my $subtype (keys %afficounts) {
	for my $af (keys %{$afficounts{$subtype}}) {
	  my($type) = $subtype . "_usage";
	  my($RRD) = "$config->{'dataDir'}/" . $subtype . "/" . $targets{$url} . "/${af}/total.rrd";
	  if (! -e $RRD) {
	    my($START) = time - $config->{'STEP'};

	    notify('info', "Creating $RRD with step $config->{'STEP'} starting at $START");
	    my($v, $msg) = RRD_create($RRD, $START, $config->{'STEP'}, $type);
	    if ($v) {
	      notify('err', "couldn't create $RRD because $msg");
	      next;
	    }
	  }

	  if (! defined $afficountsnobits{$subtype}{$af}) {
	    $afficountsnobits{$subtype}{$af} = 0;
	  }

	  my(@vals) = ($afficounts{$subtype}{$af}, $afficountsnobits{$subtype}{$af});
	  my($rv, $errmsg) = updateRRD($RRD, $collectionTime, @vals);
	  if ($rv) {
	    notify('err', "error updating $RRD : ${errmsg}");
	  }
	}
      }
    }
  } else {
    chomp($status);
    chomp($content);
    if ($status ne $content) {
      notify($level, "|${status}| could not retrieve ${targets{$url}}", $notify, ${content});
    } else {
      notify('err', "|${status}| could not retrieve ${targets{$url}}");
    }
  }
}


sub RRD_create {
  my($RRD, $START, $interval, $type) = @_;
  my(@dses);

  if (! ensureRRDDirectory($RRD)) {
    notify('crit', "Could not create ${RRD} ($!), skipping");
  }

  if ($type eq "users") {
    @dses = ("DS:users:GAUGE:600:U:U", "DS:sessions:GAUGE:600:U:U");
  } elsif ($type eq "event" 
	   || $type eq "sessionlen"
	   || $type eq "affiliate_usage" 
	   || $type eq "dept_usage") {
    @dses = ("DS:val1:ABSOLUTE:600:U:U", "DS:val2:ABSOLUTE:600:U:U");
  } elsif ($type eq "sakai_realm" 
	   || $type eq "sakai_site" 
	   || $type eq "sakai_site_tool") {
    @dses = ("DS:val1:GAUGE:600:U:U", "DS:val2:GAUGE:600:U:U");
  }
    
  RRDs::create ("$RRD", "-b", $START, "-s", $interval, @dses,
		"RRA:AVERAGE:0.5:1:43800",
		"RRA:MAX:0.5:1:43800",
		"RRA:AVERAGE:0.5:6:600",
		"RRA:MAX:0.5:6:600",
		"RRA:AVERAGE:0.5:24:600",
		"RRA:MAX:0.5:24:600",
		"RRA:AVERAGE:0.5:288:732",
		"RRA:MAX:0.5:288:732"
	       );

  if (my $error = RRDs::error()) {
    return(1, "Cannot create $RRD: $error");
  } else {
    return(0, "$RRD");
  }
}

# sub RRD_absolute_create {
#     my($RRD, $START, $interval) = @_;

#     RRDs::create ("$RRD", "-b", $START, "-s", $interval,
#                       "RRA:AVERAGE:0.5:1:43800",
#                       "RRA:MAX:0.5:1:43800",
#                       "RRA:AVERAGE:0.5:6:600",
#                       "RRA:MAX:0.5:6:600",
#                       "RRA:AVERAGE:0.5:24:600",
#                       "RRA:MAX:0.5:24:600",
#                       "RRA:AVERAGE:0.5:288:732",
#                       "RRA:MAX:0.5:288:732"
# 		  );

#     if (my $error = RRDs::error()) {
# 	return(1, "Cannot create $RRD: $error");
#     } else {
# 	return(0, "$RRD");
#     }
# }
