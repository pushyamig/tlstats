#!/usr/bin/perl -w
#
# Copyright (C) 2000-2005 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve stats from a Tomcat server
# and stuff them into an RRD.
# currently gets free / total mem and list of webapp
# status, threads, status running, status stopped, 
# context /zadmin, context /website and webappCount
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

use strict;
use FindBin;
local $| = 1;

use Time::HiRes;
use LWP::UserAgent;
use RRDs;

my $homeDir = ${FindBin::Bin}."/..";
require "$homeDir/bin/monitor.pl";

#require '/ctstats/bin/monitor.pl';

my $config = {};
$config->{'STEP'} = 300;
$config->{'DEBUG'} = 0;
$config->{'program'} = 'tcMonitor';
$config->{'version'} = "0.01";
#$config->{'homeDir'} = $ENV{'HOME'};
$config->{'homeDir'} = $homeDir;

#print "**** HACKING DATA DIR in $0 ****\n";
#$config->{'dataDir'} = $homeDir . "/data";
#$config->{'dataDir'} = "/usr/local/ctools/tmp/data";
#$config->{'dataDir'} = "/ctstats/data/tomcat";

our $sharedDataDir;
$config->{'dataDir'} = "$sharedDataDir/tomcat";

$config->{'timeout'} = 2;
if (! defined $config->{'logfacility'}){
  $config->{'logfacility'} = 'user';
}

openlog($config->{'program'},'cons,pid', $config->{'logfacility'});

my($tomcats);
while(<>){
  chomp;
  next if (/(^#)|(^$)/);
  my($server, $notify, $user, $pass, $freeMemSite, $listURL) = split(/\|/);
  $tomcats->{$server}->{'user'} = $user;
  $tomcats->{$server}->{'pass'} = $pass;
  $tomcats->{$server}->{'webappCount'} = 0;
  $tomcats->{$server}->{'freeMemSite'} = $freeMemSite;
  $tomcats->{$server}->{'listURL'} = $listURL;
  $tomcats->{$server}->{'notify'} = $notify;
}

for my $tc (sort keys %{$tomcats}){
  my($freeMemSite) = $tomcats->{$tc}->{'freeMemSite'};
  my($listSite) = $tomcats->{$tc}->{'listURL'};
  my($user) = $tomcats->{$tc}->{'user'};
  my($passwd) = $tomcats->{$tc}->{'pass'};
  my($timeout) = $config->{'timeout'}; #seconds
  my($notify) = $tomcats->{$tc}->{'notify'};

  my($rval, $code, $status, $s, $tcp, $f, $z, $content) = check_httpd($freeMemSite, $user, $passwd, $timeout);
      if ($config->{'DEBUG'}){
        my($debugContent) = "";
        my($time) = time;
        if ($config->{'DEBUG'} > 1){
	    $debugContent = $content;
        }
	print <<CONTENT;
-- CONTENT --
$time: $freeMemSite -> $rval|$code|$status|\n$debugContent
-- END CONTENT --
CONTENT
      }
  #only bother if we get real results
  if (defined($rval) && $rval > 0 && $code == 200){
    chomp($content);
    $content =~ s/\s+//g;
    ($tomcats->{$tc}->{'freeMem'}, $tomcats->{$tc}->{'totalMem'}) = split(/\|/, $content);

    my($nrval, $ncode, $nstatus, $ns, $ntcp, $nf, $nz, $ncontent) = check_httpd($listSite, $user, $passwd, $timeout);
    if ($config->{'DEBUG'}){
        my($debugContent) = "";
        my($time) = time;
        if ($config->{'DEBUG'} > 1){
	    $debugContent = $ncontent;
        }
	print <<CONTENT;
-- CONTENT --
$time: $listSite -> $nrval|$ncode|$nstatus|\n$debugContent
-- END CONTENT --
CONTENT
    }
    if (defined($nrval) && $nrval > 0 && $ncode == 200){ #only bother if we get real results
      my(@list) = split(/\n/, $ncontent);
      # lynx is not used since LWP::UserAgent can now retrieve stuff via SSL
      # my(@list) = `/usr/local/bin/lynx -dump -auth ${user}:${passwd} $tomcats->{$tc}->{'listURL'}`;
      my($i) = 1;
      for my $line (@list){
	if ($config->{'DEBUG'} > 1){
		print "${i}: $line" . "\n";
		$i++;
	}
        next unless ($line =~ /^\//);
        chomp($line);
        my($context, $status, $threads) = split(/\:/, $line);
        $tomcats->{$tc}->{'status'}->{$status}++;
        $tomcats->{$tc}->{'threads'}->{'total'} += $threads;
        $tomcats->{$tc}->{'context'}->{$context} += $threads;
        $tomcats->{$tc}->{'webappCount'}++;
        $tomcats->{$tc}->{'collectiontime'} = $f;
      }
    } else {
      notify('err', "${tc} Context list ${nstatus}", $notify, ${ncontent});
# comment out since failure of second url should not 
# throw away data from initial url.
#      $tomcats->{$tc}->{'skip'} = "true";
    }
  } else {
    notify('err', "${tc} Mem stats: ${status}", $notify, ${content});
    $tomcats->{$tc}->{'skip'} = "true";
  }
}

for my $tc (sort keys %{$tomcats}){
    next if (defined $tomcats->{$tc}->{'skip'} && $tomcats->{$tc}->{'skip'} eq "true");

    my($RRD) = "$config->{'dataDir'}/${tc}.rrd";
  
    if (! -e $RRD){
      my($START) = time - $config->{'STEP'};
  
      notify('info', "Creating $RRD with step $config->{'STEP'} starting at $START");
      my($v, $msg) = RRD_gauge_create($RRD, $START, $config->{'STEP'});
      if ($v){
        notify('err', "couldn't create $RRD because $msg");
        next;
      } 
    } 

    my($t) = $tomcats->{$tc}->{'collectiontime'} || time;  
    $t = int($t);
    my($free) = $tomcats->{$tc}->{'freeMem'} || 0;
    my($totalmem) = $tomcats->{$tc}->{'totalMem'} || 0;
    my($threads) = $tomcats->{$tc}->{'threads'}->{'total'} || 0;
    my($runningctx) = $tomcats->{$tc}->{'status'}->{'running'} || 0;
    my($stoppedctx) = $tomcats->{$tc}->{'status'}->{'stopped'} || 0;
    my($zadmintrd) = $tomcats->{$tc}->{'context'}->{'/zadmin'} || 0;
    my($websitetrd) = $tomcats->{$tc}->{'context'}->{'/website'} || 0;
    my($webappCount) = $tomcats->{$tc}->{'webappCount'} || 0;

    if (($free * 1.0) > 0 and ($totalmem * 1.0) > 0){
      my(@vals) = ($free, $totalmem, $threads, $runningctx, $stoppedctx, $zadmintrd, $websitetrd, $webappCount);
#      print "update $RRD with ",join("|",@vals),"\n";
      my($rv, $errmsg) = updateRRD($RRD, $t, @vals);
      if ($rv){
  	notify('err', "error updating $RRD : ${errmsg}");
      }

        my($percentFree) = 100 * (${free}/${totalmem});
        my($minPercent) = 5;
        if ($percentFree < $minPercent){
	    notify('crit', "${tc} has less than ${minPercent} percent free heap");
	}
    } else {
      notify('err', "${tc}: unable to retrieve valid memstats");
    }
}

sub RRD_gauge_create {
  my($RRD, $START, $interval) = @_;

  if (! ensureRRDDirectory($RRD)) {
    notify('crit', "Could not create ${RRD} ($!), skipping");
  }

  RRDs::create ("$RRD", "-b", $START, "-s", $interval,
  "DS:freemem:GAUGE:600:U:U",
  "DS:totalmem:GAUGE:600:U:U",
  "DS:threads:GAUGE:600:U:U",
  "DS:runningctx:GAUGE:600:U:U",
  "DS:stoppedctx:GAUGE:600:U:U",
  "DS:zadmintrd:GAUGE:600:U:U",
  "DS:websitetrd:GAUGE:600:U:U",
  "DS:webappcount:GAUGE:600:U:U",
  "RRA:AVERAGE:0.5:1:43800",
  "RRA:MAX:0.5:1:43800",
  "RRA:AVERAGE:0.5:6:600",
  "RRA:MAX:0.5:6:600",
  "RRA:AVERAGE:0.5:24:600",
  "RRA:MAX:0.5:24:600",
  "RRA:AVERAGE:0.5:288:732",
  "RRA:MAX:0.5:288:732"
  );

  if (my $error = RRDs::error()) {
    return(1, "Cannot create $RRD: $error");
  } else {
    return(0, "$RRD");
  }
}

