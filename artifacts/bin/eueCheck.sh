#!/bin/bash
# check that the environment is setup correctly for eue.
#
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/eueCheck.sh $
# $Id: eueCheck.sh 1907 2013-03-20 16:08:00Z dlhaines $

source ./eueMonitor.env

#CTSTATS_LIB=/usr/local/ctools/app/ctools/lib
CTSTATS_BIN=$(cd ..; pwd)/bin

## check that selenium server standalone exists

SEL_SERVER_JAR=selenium-server-standalone-2.28.0.jar
SEL_SERVER_URL=http://selenium.googlecode.com/files/$SEL_SERVER_JAR

if [ ! -e $CTSTATS_BIN/$SEL_SERVER_JAR ]; then
    echo "missing $LIB/$SEL_SERVER_JAR.  Can download using:"
    echo "(cd $CTSTATS_BIN; wget http://selenium.googlecode.com/files/selenium-server-standalone-2.28.0.jar);"
    exit 1;
fi

## check that ruby is available

RUBY=ruby
if [ -z $(which $RUBY) ]; then
    echo "can not find [$RUBY] on path: [$PATH]";
    exit 1;
fi

echo "looks ok";
#end
