#!/usr/bin/env perl
# based on ora monitor
#
# Copyright (C) 2000-2007 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve stats from a Oracle server (via a JSP)
# and stuff them into an RRD
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

#require '/ctstats/bin/monitor.pl';

# experimental version to see how to extract data from EUE script
### TTD:
### - allow reading script name from the config file
### TTD: someday
## - get away from needing changes to run on Mac for development.


use strict;
local $| = 1;

use FindBin;
use RRDs;

my $defaultScript="ctoolsRunner.rb";

my $homeDir = ${FindBin::Bin}."/..";

require "$homeDir/bin/monitor.pl";

our $sharedDataDir;

my $config = {};

$config->{'STEP'} = 300;
$config->{'DEBUG'} = 1;
#$config->{'DEBUG'} = 0;
$config->{'program'} = 'eueMonitor';
$config->{'version'} = "0.01";
$config->{'homeDir'} = $homeDir;
#print "HACK DATADIR\n";
#$sharedDataDir=".";
$config->{'dataDir'} = $sharedDataDir. "/eue";

# expect update interval to be every 5 minutes.
my $interval = 5*60;

# typical ctstats values.
my(@rras) = (
	     "RRA:AVERAGE:0.5:1:3000",
	     "RRA:MAX:0.5:1:3000",
	     "RRA:AVERAGE:0.5:5:3000",
	     "RRA:MAX:0.5:5:3000",
	     "RRA:AVERAGE:0.5:10:5000",
	     "RRA:MAX:0.5:10:5000",
	     "RRA:AVERAGE:0.5:1440:732",
	     "RRA:MAX:0.5:1440:732"
	    );

# Will track different steps in separate rrd files, so only need generic ds definition for each one.
my @dses = ("DS:val1:GAUGE:600:U:U");

### Collect information from config file about what servers to look at.
### EUE configuration format is:
### group|server
### The group is the ctools instance (ctdev, ctools, ...).
### The server is the fully qualified server name.

sub main {
  openlog($config->{'program'},'cons,pid', $config->{'logfacility'});
  my @servers;
  while (<>) {
    chomp;

    next if (/(^#)|(^$)/);

    my($group,$server,$script) = split(/\|/);
    push @servers, [$group,$server,$script];
  }


  # get results for each required server
  foreach (@servers) {
    my ($group,$server,$script) = @{$_};

    $script = $defaultScript unless($script);

    print "updating $group/$server/$script\n" if ($config->{'DEBUG'});

    # take time when start observation and use for all steps.  It's not
    # clear how to manage / graph separate times for different steps.
    # Using a single start time for the observation is clear: "observation started at time X".

    my($time) = time();
    my $r = `bash -c "$homeDir/bin/runEUEForHost.sh $server $script"` || die("can not run script: $!");

    print "r: [$r]\n" if ($config->{'DEBUG'});
    my @lines = split('\n',$r);

    processList($group,$server,$time,@lines);
  }
}

### process the results from a single server
sub processList {
  my ($group,$server,$time,@lines) = @_;
  foreach (@lines) {
    next if (m|^\s*$|);
    print "line: [$_]\n" if ($config->{'DEBUG'});
    my($tag,$secs) = m/(.+):\t(\d+\.\d+)$/;
    next if ($tag =~ /handle/);
    next if ($tag =~ /^\s*$/);
    my $RRD = getRRDName($group,$server,$tag),"\n";
    addObservation($RRD,$time,$tag,$secs) if ($tag);
  }
}

sub getRRDName {
  my ($group,$server,$tag) = @_;
  my $RRD;
  $server =~ s|\.dsc\.umich\.edu||;
  $RRD = "$config->{'dataDir'}/${group}/${server}.${tag}.rrd";
  print "rrd name: [$RRD]\n" if ($config->{'DEBUG'});
  return $RRD;
}

# pass in all the data for this observation.
# update a single step result into specific file.
sub addObservation {
  my($rrdFile,$time,$tag,$secs) = @_;
  ensureRRDDirectory($rrdFile);
  if (! -e $rrdFile) {
    createRRD($rrdFile,$interval,time-$interval,@dses);
  }
  print "r: [$rrdFile] t: [$time] tag: [$tag] secs: [$secs]\n" if ($config->{'DEBUG'});
  my $rc = updateRRD($rrdFile,$time,$secs);
  unless ($rc =~ /OK/i) {
		print "updateRRD error: $rc\n";
	    }
}

########### Idiom to allow running a script standalone or let it be read and
########### tested from a standard perl test script.

### Run the script unless it is called from a test file.
print main(@ARGV) unless ( $0 =~ m|.*\.t| );

# Need to return 1 if called from a test file.
1;

#end
