#!/bin/bash
# Make xml versions of rrd files from a list of files.
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/rrdDumpTree.sh $
# $Id: rrdDumpTree.sh 2146 2015-06-04 16:50:55Z dlhaines $

#set -x -u -e
#set -x 
set -u -e

#perl -pi -e 's/<version>1.1.14/<version>ct-1.1.14-01/g' `find . -name \*.xml`

# for f in `find ${MUNIN_DIR} -name '*.xml' -print` ;
# do
# f_rrd=`dirname ${f}`/`basename ${f} .xml`.rrd
# mv -f "${f_rrd}" "${f_rrd}.bak"
# #chown root:0 "${f_rrd}.bak"
# rrdtool restore "$f" "${f_rrd}"
# #chown munin:munin "${f_rrd}"
# done

EXT="rrd";

#RRD_FILES=$(find . -name \*.xml)
RRD_FILES=$(find . -name \*.$EXT)

for name in $RRD_FILES; do
#    $prefix = ${$name%.rrd};
    prefix=${name%.$EXT};
#    echo "prefix [$prefix]";
#    echo "prefixxml [$prefix.xml]";
    rrdtool dump $prefix.rrd >| $prefix.xml
    gzip $prefix.xml
    rm $prefix.rrd
#    ls -l $prefix.*;
done


#end
