# Copyright (C) 2000-2005 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.

### TTD: (things to do)
### - generalize the RRD create/update methods so the specifics are passed in.
### - adopt getRRDName from eueMonitor.pl
### -- make rrd name take list of groups and have them map to directory structure.
### -- make that work with ensureRRDDirectory

use strict;
local $| = 1;

#use vars qw($config);
our $config;

use Digest::MD5  qw(md5 md5_hex md5_base64);
use Time::HiRes;
use LWP::UserAgent;
use Sys::Syslog;
Sys::Syslog::setlogsock('unix');
use RRDs;
use File::Path;

our $writeDataToFile=0;

##### shared variables
# Common data directory specified here so that 
# can be changed for all scripts in one place.
# This is a testing directory.
#our $sharedDataDir="/usr/local/ctools/ctstats-wush/ctstats-drraw/data";
our $sharedDataDir="/usr/local/ctools/app/ctools/data";

$config->{'STEP'} = 300;
$config->{'DEBUG'} = 0;
$config->{'timeout'} = 15;
if (! defined $main::config->{'logfacility'}){
    $config->{'logfacility'} = 'user';
}

sub check_httpd {
    my($url, $username, $pwd, $timeout) = @_;
    my($totaltimeout) = $timeout + 3;
    my($rval) = 0;

    my($ua) = new LWP::UserAgent;
    $ua->timeout($timeout);

    my($start) = Time::HiRes::time ();
    my($request) = HTTP::Request->new(GET => $url);
    my($tcp_end) = Time::HiRes::time ();

    ### May need to get rid of this / check presence of properties file??
    # no need to send basic auth if we don't have a username and pwd defined
    if ($username ne "" && $pwd ne ""){
	$request->authorization_basic($username, $pwd);
    }

    my($response);
    # move to count the request processing time also.
    #my($finish) = Time::HiRes::time ();

    eval {
	local $SIG{ALRM} = sub { die "alarm\n" };
	alarm($totaltimeout);
	$response = $ua->request($request);
	alarm(0);
    };

    my($finish) = Time::HiRes::time ();
    return (500, 500, "timeout alarm", $start, $tcp_end, $finish, 0, "", 0) if $@ eq "alarm\n";

    my($content) = $response->content;
    $rval = $response->is_success - $response->is_error;
    my($contentsize) = length($content);
    my($md5) = md5_base64($content);

    undef $ua;
    return($rval, $response->code, $response->status_line, $start, $tcp_end, $finish, $contentsize, $content, $md5);
}

sub updateRRD {
    my($RRD, $t, @vals) = @_;

    print "monitor.pl: updateRRD\n" if ($config->{'DEBUG'});
    if (! -e $RRD){
	return(1, "could not find ${RRD}");
    } else {
	my($vallist) = $t . ":" . join(':', @vals);

	if ($writeDataToFile) {
	  open OUT, ">>", "$RRD.txt" or die "Couldn't open file: $RRD.txt $!\n";
	  print OUT "$vallist\n";
	  close OUT;
	}

	RRDs::update("$RRD", "$vallist");
	if (my $error = RRDs::error()) {
	    warn("rrd update error: $error");
	    return(1, "RRDs::update - $RRD [$error] (${vallist})");
	} else {
	    return(0, "OK");
	}
    }
}


sub notify {
  #notifyReal(@_);
  notifyDebug(@_);
}

## notify redefined for debugging.   Use real one for runns
sub notifyDebug {
  print "notify: ",join("|",@_),"\n";
}

sub notifyReal {
    my($severity, $mesg, $who, $longmsg) = @_;
    $mesg =~ s/\%//g;
    if (! $who){
	$who = "";
    }
    if (! $longmsg){
	$longmsg = $mesg;
    }
    $longmsg =~ s/\%//g;
    my($useverity) = uc($severity);

    my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    $year += 1900;
    $mon += 1;
    my($timestamp) = sprintf("%02d-%02d %02d:%02d:%02d", $mon, $mday, $hour, $min, $sec);
    my($pid) = $$;

    if ($severity eq "debug"){
	if ($config->{'DEBUG'}){
	    print STDERR "${useverity}: ($timestamp): $mesg\n";
	}
    } else {
	syslog($severity, "${useverity}: $mesg");
	if ($severity eq "emerg" || $severity eq "crit"){
	    if ($who ne "" && ($mesg ne $longmsg)){
                                my($rv, $errmsg) = &sendmail($who, "watsopmon\@umich.edu", "$mesg (${useverity})", $longmsg);
                                if ($rv){
				    syslog('crit', "CRIT: could not send email to ${who} (${errmsg})");
                                }
			    }
	}
    }
}

sub sendmail {
    my($to, $from, $subject, $msg) = @_;
    if (! $msg){
	$msg = $subject;
    }

    open MAIL,"| /usr/sbin/sendmail -t -oi" or return (1, "Couldn't pipe to sendmail: $!");

    print MAIL <<"MAIL";
To: ${to}
From: ${from}
Reply-To: ${from}
Subject: ${subject}

-------------8<----------cut here----------------8<-----------------
    ${msg}
-------------8<----------cut here----------------8<-----------------
MAIL

    close MAIL or return(1, "Couldn't close sendmail pipe: $!");
return(0, "okay");
}

# Recursively print a hash with an option identification tag.
sub printHash {
  my $h = shift;
  my $tag = shift;
  foreach (sort(keys %{$h})) {
#    if (""HASH" eq ref($_)
    if ($tag) {
      print "$tag ";
    }
    print "k: [$_] ";
    my $v = $h->{$_};
    if ("HASH" eq ref($v)) {
      print "-", printHash($v);
    }
    else {
      print "v: [$h->{$_}]\n";
    }
  }
}

# Slurp an entire file into an array.  Don't do this 
# with a big file!!

sub returnFileAsArray {
  my $filename = shift;
  open(FILE, $filename) or die("Unable to open file: [$filename] $!");
  # read entire file into an array.
  my @lines = <FILE>;
  close(FILE);
  return @lines;
}


## Create an rrd file directory if necessary

sub ensureRRDDirectory {
  my $rrdFullName = shift;
  my $rrdDir = $rrdFullName;
  $rrdDir =~ s|/[^/]+\.rrd$||;

  if ( -d $rrdDir ) {
    return "$rrdFullName";
  }

  if (! mkpath("${rrdDir}",1, 0755)) {
    notify('crit', "Could not create ${rrdDir} ($!), skipping");
    return undef;
  }

  return "$rrdFullName";
}

sub createRRD {
  my($RRD,$interval,$START,@dses) = @_;
#  print "createRRD2\n";
  notify('info', "Creating $RRD with step ${interval} starting at $START");
  RRDs::create ("$RRD", "-b", $START, "-s", $interval, @dses,
		"RRA:AVERAGE:0.5:1:43800",
		"RRA:MAX:0.5:1:43800",
		"RRA:AVERAGE:0.5:6:600",
		"RRA:MAX:0.5:6:600",
		"RRA:AVERAGE:0.5:24:600",
		"RRA:MAX:0.5:24:600",
		"RRA:AVERAGE:0.5:288:732",
		"RRA:MAX:0.5:288:732"
	       );
  if (my $error = RRDs::error()) {
    return(1, "Cannot create $RRD: $error");
  } else {
    return(0, "$RRD");
  }
}

# Redefine open log for debugging.  Comment this out
# if want to use system logs
sub openlog {
  print "log: ",join("|",@_),"\n";
}


1;
