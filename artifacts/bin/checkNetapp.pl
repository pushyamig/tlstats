#!/usr/bin/perl -w
#
# Copyright (C) 2000-2006 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve stats from a netapp
# and stuff them into an RRD
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

use strict;
local $| = 1;

use Getopt::Std;
my(%opts);
getopts('D:', \%opts);		#Values in %opts

use FindBin;
my $homeDir = ${FindBin::Bin}."/..";

require "$homeDir/bin/monitor.pl";
require "$homeDir/bin/netappLib.pl";

our $sharedDataDir;

#require '/ctstats/bin/monitor.pl';
#require '/ctstats/bin/netappLib.pl';

my($config);
$config->{'program'} = "checkNetapp";
$config->{'version'} = "0.01";

#$config->{'homeDir'} = $ENV{'HOME'};
$config->{'homeDir'} = $homeDir;

$config->{'libDir'} = $config->{'homeDir'} . "/lib";
$config->{'mibDir'} = $config->{'libDir'} . "/mibs";

$config->{'dataDir'} = "$sharedDataDir/netapp";

if (! -d "$config->{'dataDir'}") {
  if (! mkdir "$config->{'dataDir'}", 0750) {
    notify('crit', "Could not create $config->{'dataDir'} ($!), exiting");
    exit 1;
  }
}


if (! defined $config->{'logfacility'}) {
  $config->{'logfacility'} = 'user';
}
$config->{'STEP'} = 300;
$config->{'DEBUG'} = $opts{'D'} || 0;
$config->{'timeout'} = 10;

if ($config->{'DEBUG'}) {
  print STDERR "DEBUG mode enabled!\n";
}

$config->{'netappGetStats'} = 0;

openlog($config->{'program'},'cons,pid', $config->{'logfacility'});

use lib qw($config->{'libDir'});

use BER;
$BER::pretty_print_timeticks = 0;
use SNMP_util;
if ($config->{'DEBUG'}) {
  $SNMP_Session::suppress_warnings = 0;
} else {
  $SNMP_Session::suppress_warnings = 2;
}
snmpLoad_OID_Cache($SNMP_util::CacheFile);

opendir(DIR, $config->{'mibDir'}) || die "can't opendir $config->{'mibDir'}: $!";
my(@files) =readdir(DIR);
closedir DIR;

for my $mib (@files) {
  chomp($mib);
  $mib = $config->{'mibDir'} . "/" . $mib;
  next if ($mib =~ /^\./);	#skip dot files
  next unless ($mib =~ /\.mib/); #skip any files that don't have mib in their name
  my($rv) = &snmpQueue_MIB_File($mib);
  #   my($rv) = &snmpMIB_to_OID($mib);
  if ($rv && $config->{'DEBUG'}) {
    print STDERR "* ERROR: Could not load $mib: $rv\n";
  }
}

my(@trendoids) = (
		  'miscHighNetRcvdBytes',
		  'miscLowNetRcvdBytes',
		  'tnfsServBadCalls',
		  'miscLowDiskReadBytes',
		  'trpcBadLens',
		  'pclNumber',
		  'diskActiveCount',
		  'cpuUpTime',
		  'miscLowNetSentBytes',
		  'cpuBusyTimePerCent',
		  'miscCacheAge',
		  'miscHighDiskReadBytes',
		  'trpcServXDRCalls',
		  'miscHighNetSentBytes',
		  'diskSpareCount',
		  'miscHighNfsOps',
		  'trpcCalls',
		  'tnfsCalls',
		  'trpcBadCalls',
		  'diskTotalCount',
		  'miscHighDiskWriteBytes',
		  'miscLowDiskWriteBytes',
		  'miscLowNfsOps',
		  'spareNumber',
		  'trpcNullRecvs',
		 );

my(@pcltables) = (
		  'pclNfsCalls',
		  'pclNfsServBadCalls',
		  'pclPerCent',
		 );

my(@dftables) = (
		 'dfFileSys',
		 'dfHighTotalKBytes',
		 'dfLowTotalKBytes',
		 'dfInodesUsed',
		 'dfInodesFree',
		 'dfMaxFilesPossible',
		 'dfMaxFilesUsed',
		 'dfHighUsedKBytes',
		 'dfLowUsedKBytes',
		 'dfHighAvailKBytes',
		 'dfLowAvailKBytes',
		);

my(@smtables) = (
		 "snapmirrorSrc",
		 "snapmirrorDst",
		 "snapmirrorLag",
		 "snapmirrorTotalSuccesses",
		 "snapmirrorTotalRestartSuccesses",
		 "snapmirrorTotalFailures",
		 "snapmirrorTotalDeferments",
		 "snapmirrorTotalTransMBs",
		 "snapmirrorTotalTransTimeSeconds",
		 "snapmirrorThrottleValue",
		 "snapmirrorSyncToAsync",
		);

my(@sisoids) = (
		"sisPath",
		"sisHighLastOpSize",
		"sisLowLastOpSize",
		"sis64LastOpSize",
	       );



my($targets);
while (<>) {
  if (/(^$)|(^#)/) {
    next;
  }
  chomp;
  if (/^config-var\|/) {
    my($type, $var, $value) = split(/\|/);
    if ($type eq "config-var") {
      notify('debug', "setting config::${var} to ${value}");
      $config->{$var} = $value;
    }
  } else {
    my($netapp, $rocommunity, $spares, $smenabled) = split(/\|/);
    $targets->{$netapp}->{'rocommunity'} = $rocommunity;
  }
}

for my $device (keys %{$targets}) {
  $config->{'device'} = $device;
  my($agent) = "$targets->{$device}->{'rocommunity'}\@$config->{'device'}";

  if (! -d "$config->{'dataDir'}/${device}") {
    if (! mkdir "$config->{'dataDir'}/${device}", 0750) {
      notify('crit', "Could not create $config->{'dataDir'}/${device} ($!), skipping");
      next;
    }
  }



  sub update {
    if (@_) {
    } else {
      return (1, "no values!");
    }
    my($type) = shift(@_);
    my($first) = shift(@_);

    my($name, $RRD);
    if ($type eq "df" || $type eq "sis") {
      $name = shift(@_);
      $name =~ s/^\///;
      $name =~ s/\/$//;
      $name =~ s/\//_/g;
      #$name = $first . "." . $name;

      $RRD = $config->{'dataDir'} . "/" . $config->{'device'} . "/${type}." . $name . ".rrd";
    } elsif ($type eq "trend") {
      $RRD = $config->{'dataDir'} . "/" . $config->{'device'} . "/trendoids" . ".rrd";	
    } elsif ($type eq "pcl") {
      my($pclip) = $first;
      $RRD = $config->{'dataDir'} . "/" . $config->{'device'} . "/${pclip}" . ".rrd";
    } elsif ($type eq "snapmirror") {
      my($src) = shift(@_);
      my($dst) = shift(@_);
      $RRD = $config->{'dataDir'} . "/" . $config->{'device'} . "/${type}-" . $src . "-" . $dst . ".rrd";
    } elsif ($type eq "volStats" || $type eq "diskStats") {
      my($name) = shift(@_);
      $name =~ s/\:/\-/g;
      $RRD = $config->{'dataDir'} . "/" . $config->{'device'} . "/${type}/" . $name . ".rrd";
    }

    my(@vals) = @_;
	
    for (my $i=0;$i<=$#vals;$i++) {
      if (! $vals[$i] || $vals[$i] eq '') {
	$vals[$i] = 0;
      }
      my $timeval = $vals[$i];
      chomp($timeval);
      if ($timeval =~ /^\d*\:\d+\:\d+$/) {
	$timeval =~ s/(\d*)\:(\d+)\:(\d+)//;
	my ($hrs, $mins, $secs) = ($1, $2, $3);
	$vals[$i] = $hrs * 3600 + $mins * 60 + $secs;
      }
    }

    if (! -e $RRD) {
      my($START) = time - $config->{'STEP'};
  
      notify('info', "Creating $RRD with step $config->{'STEP'} starting at $START");
      my($v, $msg) = RRD_create($RRD, $START, $config->{'STEP'}, $type);
      if ($v) {
	notify('err', "not creating |${RRD}| for type ${type} because |${msg}|");
	return;
      } 
    } 

    my($t) = time;
    $t = int($t);
    my($rv, $errmsg) = updateRRD($RRD, $t, @vals);
    if ($rv) {
      notify('err', $errmsg);
    }
  }

  sub RRD_create {
    my($RRD, $START, $interval, $type) = @_;
    my(@dses);

    if ($type eq "trend") {
      @dses = (
	       "DS:mischighnetrcvdbyte:COUNTER:600:U:U",
	       "DS:misclownetrcvdbytes:COUNTER:600:U:U",
	       "DS:tnfsservbadcalls:COUNTER:600:U:U",
	       "DS:misclowdiskreadbyte:COUNTER:600:U:U",
	       "DS:trpcbadlens:COUNTER:600:U:U",
	       "DS:pclnumber:GAUGE:600:U:U",
	       "DS:diskactivecount:GAUGE:600:U:U",
	       "DS:cpuuptime:COUNTER:600:U:U",
	       "DS:misclownetsentbytes:COUNTER:600:U:U",
	       "DS:cpubusytimepercent:GAUGE:600:U:U",
	       "DS:misccacheage:GAUGE:600:U:U",
	       "DS:mischighdiskreadbyt:COUNTER:600:U:U",
	       "DS:trpcservxdrcalls:COUNTER:600:U:U",
	       "DS:mischighnetsentbyte:COUNTER:600:U:U",
	       "DS:disksparecount:GAUGE:600:U:U",
	       "DS:mischighnfsops:COUNTER:600:U:U",
	       "DS:trpccalls:COUNTER:600:U:U",
	       "DS:tnfscalls:COUNTER:600:U:U",
	       "DS:trpcbadcalls:COUNTER:600:U:U",
	       "DS:disktotalcount:GAUGE:600:U:U",
	       "DS:mischighdiskwriteby:COUNTER:600:U:U",
	       "DS:misclowdiskwritebyt:COUNTER:600:U:U",
	       "DS:misclownfsops:COUNTER:600:U:U",
	       "DS:sparenumber:GAUGE:600:U:U",
	       "DS:trpcnullrecvs:COUNTER:600:U:U",
	      );
    } elsif ($type eq "pcl") {
      @dses = (
	       "DS:nfscalls:COUNTER:600:U:U",
	       "DS:nfsservbadcalls:COUNTER:600:U:U",
	       "DS:percent:GAUGE:600:U:U",
	      );
    } elsif ($type eq "df") {
      @dses = (
	       "DS:dfhitotkb:GAUGE:600:U:U",
	       "DS:dflototkb:GAUGE:600:U:U",
	       "DS:dfinodesused:GAUGE:600:U:U",
	       "DS:dfinodesfree:GAUGE:600:U:U",
	       "DS:dfmaxfiles:GAUGE:600:U:U",
	       "DS:dfmaxfilesused:GAUGE:600:U:U",
	       "DS:dfhiusedkb:GAUGE:600:U:U",
	       "DS:dflousedkb:GAUGE:600:U:U",
	       "DS:dfhiavailkb:GAUGE:600:U:U",
	       "DS:dfloavailkb:GAUGE:600:U:U",
	      );
    } elsif ($type eq "snapmirror") {
      @dses = (
	       "DS:smlag:COUNTER:600:U:U",
	       "DS:smsuccesses:COUNTER:600:U:U",
	       "DS:smrestartsuccs:COUNTER:600:U:U",
	       "DS:smfailures:COUNTER:600:U:U",
	       "DS:smdeferments:COUNTER:600:U:U",
	       "DS:smtransmb:COUNTER:600:U:U",
	       "DS:smtranstime:COUNTER:600:U:U",
	       "DS:smthrottle:GAUGE:600:U:U",
	       "DS:smsynctoasync:GAUGE:600:U:U",
	      );
    } elsif ($type eq "volStats") {
      @dses = (
	       "DS:rdata:GAUGE:600:U:U",
	       "DS:rlatency:GAUGE:600:U:U",
	       "DS:rops:GAUGE:600:U:U",
	       "DS:wdata:GAUGE:600:U:U",
	       "DS:wlatency:GAUGE:600:U:U",
	       "DS:wops:GAUGE:600:U:U",
	       "DS:olatency:GAUGE:600:U:U",
	       "DS:oops:GAUGE:600:U:U",
	      );
    } elsif ($type eq "diskStats") {
      @dses = (
	       "DS:ureads:GAUGE:600:U:U",
	       "DS:urchain:GAUGE:600:U:U",
	       "DS:urblocks:GAUGE:600:U:U",
	       "DS:urlatency:GAUGE:600:U:U",
	       "DS:uwrites:GAUGE:600:U:U",
	       "DS:uwchain:GAUGE:600:U:U",
	       "DS:uwblocks:GAUGE:600:U:U",
	       "DS:uwlatency:GAUGE:600:U:U",
	       "DS:diskbusy:GAUGE:600:U:U"
	      );
    } elsif ($type eq "sis") {
      @dses = (
	       "DS:hilastopsize:GAUGE:600:U:U",
	       "DS:lolastopsize:GAUGE:600:U:U",
	       "DS:lastopsize:COUNTER:600:U:U"
	      );
    } else {
      notify('ERR', "could not create RRD of type ${type}");
      return(1, "do not recognize type ${type}");
    }

    RRDs::create ("$RRD", "-b", $START, "-s", $interval, @dses,
		  "RRA:AVERAGE:0.5:1:43800",
		  "RRA:MAX:0.5:1:43800",
		  "RRA:AVERAGE:0.5:6:600",
		  "RRA:MAX:0.5:6:600",
		  "RRA:AVERAGE:0.5:24:600",
		  "RRA:MAX:0.5:24:600",
		  "RRA:AVERAGE:0.5:288:732",
		  "RRA:MAX:0.5:288:732"
		 );

    if (my $error = RRDs::error()) {
      return(1, "Cannot create $RRD: $error");
    } else {
      return(0, "$RRD");
    }
  }

  sub trend2rrd {update('trend', @_);}
  sub df2rrd {update('df', @_);}
  sub pcl2rrd {update('pcl', @_);}
  sub sm2rrd {update('snapmirror', @_);}
  sub sis2rrd {
    update('sis', @_);
  }

  my(%sets);
  $sets{"trend"}{"cols"} = \@trendoids;
  $sets{"trend"}{"func"} = \&trend2rrd;
  $sets{"df"}{"cols"} = \@dftables;
  $sets{"df"}{"func"} = \&df2rrd;
  $sets{"pcl"}{"cols"} = \@pcltables;
  $sets{"pcl"}{"func"} = \&pcl2rrd;
  $sets{"sm"}{"cols"} = \@smtables;
  $sets{"sm"}{"func"} = \&sm2rrd;

  # possible problem running "sis status" according to BURTs:
  # http://now.netapp.com/NOW/cgi-bin/bol?Type=Detail&Display=349251
  # http://now.netapp.com/NOW/cgi-bin/bol?Type=Detail&Display=346831
  # workaround for both:
  # Avoid running any "SIS status" related reports from DFM or                                                          
  #"SIS status" executions from FilerView.  As an alternative you can run
  #SIS status related commands from the CLI.
  #
  #    $sets{"sis"}{"cols"} = \@sisoids;
  #    $sets{"sis"}{"func"} = \&sis2rrd;


  for my $set (keys %sets) {
    #my($rv) = &snmpmaptable($agent, $sets{$set}{"func"}, { 'use_getbulk' => 0 }, @{$sets{$set}{"cols"}});
    my($rv) = &snmpmaptable($agent, $sets{$set}{"func"}, @{$sets{$set}{"cols"}});
    if (!defined($rv) || $rv < 0) {
      notify('err', "getting ${set} from ${device}");
    }
  }

  # get netapp-specific stats via sshd
  my($getStatCols);

  $getStatCols->{'disk'}->{'type'} = "diskStats";
  @{$getStatCols->{'disk'}->{'cols'}} = ("user_reads" ,"user_read_chain" ,"user_read_blocks" ,"user_read_latency" ,"user_writes" ,"user_write_chain" ,"user_write_blocks" ,"user_write_latency" ,"disk_busy");

  $getStatCols->{'volume'}->{'type'} = "volStats";
  @{$getStatCols->{'volume'}->{'cols'}} = ("read_data", "read_latency", "read_ops", "write_data", "write_latency", "write_ops", "other_latency", "other_ops");

  if ($config->{'netappGetStats'}) {
    for my $stat (keys %{$getStatCols}) {
      my($stattxt) = "${stat}:*:" . join(" ${stat}:*:", @{$getStatCols->{$stat}->{'cols'}});
      getStatTable("ro", $config->{'device'}, $getStatCols->{$stat}->{'type'}, $stattxt, \&update);
    }
  }

  #    getStatTable("ro", $config->{'device'}, "volStats", "volume:*:read_data volume:*:read_latency volume:*:read_ops volume:*:write_data volume:*:write_latency volume:*:write_ops volume:*:other_latency volume:*:other_ops", \&update);
  #disk:*:user_reads disk:*:user_read_chain disk:*:user_read_blocks disk:*:user_read_latency disk:*:user_writes disk:*:user_write_chain disk:*:user_write_blocks disk:*:user_write_latency disk:*:disk_busy

  #    snmpmaptable($agent, \&trend2rrd, @trendoids);
  #    snmpmaptable($agent, \&df2rrd, @dftables);
  #    snmpmaptable($agent, \&pcl2rrd, @pcltables);
  #    snmpmaptable($agent, \&sm2rrd, @smtables);

}

closelog();
