#!/bin/bash
# Restore rrd files from compressed xml rrd dump files. Removes original file.
#
# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/rrdRestoreTree.sh $
# $Id: rrdRestoreTree.sh 2146 2015-06-04 16:50:55Z dlhaines $

#set -x -u -e
#set -x 
set -u -e

#perl -pi -e 's/<version>1.1.14/<version>ct-1.1.14-01/g' `find . -name \*.xml`

# for f in `find ${MUNIN_DIR} -name '*.xml' -print` ;
# do
# f_rrd=`dirname ${f}`/`basename ${f} .xml`.rrd
# mv -f "${f_rrd}" "${f_rrd}.bak"
# #chown root:0 "${f_rrd}.bak"
# rrdtool restore "$f" "${f_rrd}"
# #chown munin:munin "${f_rrd}"
# done

EXT="xml.gz";

FILES=$(find . -name \*.$EXT)

for name in $FILES; do
    gzip -d $name
    prefix=${name%.$EXT};
    rrdtool restore $prefix.xml $prefix.rrd
    rm $prefix.xml
 #     ls -l $prefix.*;
done


#end
