#!/usr/bin/perl -w
#
# Copyright (C) 2000-2007 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve stats from a Oracle server (via a JSP)
# and stuff them into an RRD
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

#require '/ctstats/bin/monitor.pl';

use strict;
local $| = 1;

use FindBin;
use RRDs;

my $homeDir = ${FindBin::Bin}."/..";

require "$homeDir/bin/monitor.pl";

our $sharedDataDir;

my $config = {};

$config->{'STEP'} = 300;
$config->{'DEBUG'} = 0;
$config->{'program'} = 'oraMonitor';
$config->{'version'} = "0.01";
#$config->{'homeDir'} = $ENV{'HOME'};
$config->{'homeDir'} = $homeDir;
#$config->{'dataDir'} = $ENV{'HOME'} . "/data/db";
$config->{'dataDir'} = $sharedDataDir. "/db";

openlog($config->{'program'},'cons,pid', $config->{'logfacility'});

my $instances = {};
while(<>){
  chomp;

  next if (/(^#)|(^$)/);

  my($server, $notify, $user, $pass, $statUrl, $dbname) = split(/\|/);
  $instances->{$dbname}->{'user'} = $user;
  $instances->{$dbname}->{'pass'} = $pass;
  $instances->{$dbname}->{'statUrl'} = $statUrl;
  $instances->{$dbname}->{'dbname'} = $dbname;
  $instances->{$dbname}->{'notify'} = $notify;

}

#printHash($instances,"oraMonitor");

for my $tc (sort keys %{$instances}){
  my($statUrl) = $instances->{$tc}->{'statUrl'};
  my($dbname) = $instances->{$tc}->{'dbname'};
  my($user) = $instances->{$tc}->{'user'};
  my($passwd) = $instances->{$tc}->{'pass'};
  my($timeout) = 60;
  my($notify) = $instances->{$tc}->{'notify'};

  if ($user ne "" && $passwd ne ""){
  } else {
      #notify('INFO', "$$ no username/password to send so not doing basic auth");
  }
    my($nrval, $ncode, $nstatus, $ns, $ntcp, $nf, $nz, $ncontent) = check_httpd($statUrl, $user, $passwd, $timeout);
  $instances->{$tc}->{'collectiontime'} = time;
    if ($config->{'DEBUG'}){
        my($debugContent) = "";
        my($time) = time;
        if ($config->{'DEBUG'} > 1){
	    $debugContent = $ncontent;
        }
	print <<CONTENT;
-- CONTENT --
$time: $dbname -> $nrval|$ncode|$nstatus|\n$debugContent
-- END CONTENT --
CONTENT
    }
    if (defined($nrval) && $nrval > 0 && $ncode == 200){ #only bother if we get real results
      my(@list) = split(/\n/, $ncontent);
      my($i) = 1;
      for my $line (@list){
	if ($config->{'DEBUG'} > 1){
		print "${i}: $line" . "\n";
	}
	$i++;

	if ($line =~ /java.sql.SQLException/){
	    notify('err', "${tc}: encountered ${line} -- skipping");
	    next;
	}
        chomp($line);
	#	print "line: [$line]\n";
	#next if ($line =~ ((/^$/)|(/^\s+$/)|(/^#/)));
        next unless ($line =~ /\w+\|/);
        my($type, $stat, @vals) = split(/\|/, $line);

	my($orig_stat) = $stat;
	my($min_stat) = $stat;

	$min_stat =~ s/\//-/g;

	$stat =~ s/^\s+//g;
	$stat =~ s/\s+$//g;
	$stat =~ s/\*//g;
	$stat =~ s/\(//g;
	$stat =~ s/\)//g;
	$stat =~ s/\//-/g;
	$stat =~ s/\$/\-/g;
	$stat =~ s/\%/pc/g;
	$stat =~ s/timeout/to/g;
	$stat =~ s/Per Sec/ps/g;
	$stat =~ s/Per Txn/pt/g;
	$stat =~ s/Ratio/r/g;
	$stat =~ s/Physical/phys/g;
	$stat =~ s/User/usr/g;
	$stat =~ s/Transaction/txn/g;
	$stat =~ s/Database/db/g;
	$stat =~ s/Requests/req/g;
	$stat =~ s/Response/resp/g;
	$stat =~ s/Receive/recv/g;
	$stat =~ s/Deadlock/ddlck/g;
	$stat =~ s/Allocation/alloc/g;
	$stat =~ s/Current/cur/g;
	$stat =~ s/Executions/exn/g;
	$stat =~ s/Table/tbl/g;
	$stat =~ s/Write/wrt/g;
	$stat =~ s/Direct/dir/g;
	$stat =~ s/read/r/ig;
	$stat =~ s/consistent/cons/ig;
	$stat =~ s/rollback/rbk/ig;
	$stat =~ s/downgrade/dwngr/ig;
	$stat =~ s/create/crt/ig;
	$stat =~ s/service/svc/ig;
	$stat =~ s/global/glb/ig;
	$stat =~ s/generate/gen/ig;
	$stat =~ s/change/cng/ig;
	$stat =~ s/utilization/utl/ig;
	$stat =~ s/without/wo/ig;
	$stat =~ s/total/tot/ig;
	$stat =~ s/Library/lib/g;
	$stat =~ s/Block/blk/g;
	$stat =~ s/Logical/logi/g;
	$stat =~ s/Limit/lmt/g;
	$stat =~ s/enqueue/enq/g;
	$stat =~ s/applied/appl/g;
	$stat =~ s/application/app/g;
	$stat =~ s/\:/\-/g;
	$stat =~ s/\*//g;
	$stat =~ s/\&/and/g;
	$stat =~ s/\s+/_/g;
	$stat =~ s/\'//g;
	$stat = lc($stat);

        my($t) = time;  
        $t = int($t);

        my($RRD);
	if ($type eq "dbmetric" || $type eq "sys"){
	    $RRD = "$config->{'dataDir'}/${dbname}-${stat}.rrd";
	    pop(@vals);
	} elsif ($type eq "tblsp"){
	    $RRD = "$config->{'dataDir'}/tblsp/${dbname}-${stat}.rrd";
	    pop(@vals);
	} elsif ($type eq "table"){
	    $RRD = "$config->{'dataDir'}/table/${dbname}-${stat}.rrd";
	} elsif (
		 $type eq "session" 
		 || $type eq "active-session" 
		 || $type eq "waiting-session"
		 || $type eq "session-mem"
		 ){
	    $RRD = "$config->{'dataDir'}/${type}/${dbname}-${stat}.rrd";
	} elsif ($type eq "sql_id" || $type eq "app-sql_id"){
	    $RRD = "$config->{'dataDir'}/${type}/${dbname}-${orig_stat}.rrd";
	} elsif ($type eq "eventwait" || $type eq "px_stat"){
	    $RRD = "$config->{'dataDir'}/${type}/${dbname}/${stat}.rrd";
	} elsif ($type eq "object_access" || $type eq "table_dml" || $type eq "sql_wait"){
	    $RRD = "$config->{'dataDir'}/${type}/${dbname}/${min_stat}.rrd";
	    $t = shift(@vals);
	} elsif ($type eq "sesstat"){
	    my($user) = shift(@vals);
	    $RRD = "$config->{'dataDir'}/${type}/${dbname}/${user}-${stat}.rrd";
	} else {
	    notify('warning', "${tc}: unknown type |${type}|, skipping", $notify, $line);
	    next;
	}
  
        if (! -e $RRD){
	  ensureRRDDirectory($RRD);
	    if (! -d "$config->{'dataDir'}/${type}"){
		if(! mkdir "$config->{'dataDir'}/${type}", 0755){
		    notify('crit', "Could not create $config->{'dataDir'}/${type} ($!), skipping");
		    next;
		}
	    }

	    if (! -d "$config->{'dataDir'}/${type}/${dbname}"){
		if(! mkdir "$config->{'dataDir'}/${type}/${dbname}", 0755){
		    notify('crit', "Could not create $config->{'dataDir'}/${type}/${dbname} ($!), skipping");
		    next;
		}
	    }


           my($START) = time - $config->{'STEP'};
	   my($v, $msg) = RRD_create($RRD, $START, $config->{'STEP'}, $type, $stat);
           if ($v){
             notify('err', "($RRD) $msg");
             next;
          } 
        } 

        my($rv, $errmsg) = updateRRD($RRD, $t, @vals);
        if ($rv){
	    if ($errmsg =~ /minimum one second step/){
	    } else {
		notify('err', "error updating $RRD : ${errmsg}");
	    }
        }

      }
    } else {
      notify('err', "${tc} Context list ${nstatus}", $notify, ${ncontent});
      $instances->{$tc}->{'skip'} = "true";
    }

}

sub RRD_create {
    my($RRD, $START, $interval, $type, $stat) = @_;
    my(@dses);

    ensureRRDDirectory($RRD);

    if ($type eq "dbmetric" || $type eq "sys"){
#	my($dsstat) = substr($stat, 0, 19);
#	@dses = ("DS:${dsstat}:GAUGE:600:U:U");
	@dses = ("DS:val1:GAUGE:600:U:U");
    } elsif ($type eq "tblsp"){
	@dses = ("DS:val1:GAUGE:600:U:U");
    } elsif ($type eq "table"){
	@dses = ("DS:rows:GAUGE:600:U:U", "DS:blocks:GAUGE:600:U:U");
    } elsif ($type eq "session"){
	@dses = ("DS:active:GAUGE:600:U:U", "DS:inactive:GAUGE:600:U:U");
    } elsif ($type eq "active-session"){
	@dses = ("DS:active:ABSOLUTE:600:U:U", "DS:inactive:ABSOLUTE:600:U:U");
    } elsif ($type eq "session-mem"){
	@dses = ("DS:val0:GAUGE:600:U:U", "DS:val1:GAUGE:600:U:U");
    } elsif ($type eq "eventwait"){
	@dses = ("DS:totalwaits:COUNTER:3600:U:U", "DS:timewaitedmicro:COUNTER:3600:U:U", "DS:timeouts:COUNTER:3600:U:U");
    } elsif ($type eq "sesstat"){
	@dses = ("DS:sessions:GAUGE:600:U:U", "DS:val0:COUNTER:600:U:U");
    } elsif ($type eq "px_stat"){
	@dses = ("DS:val0:COUNTER:600:0:U");
    } elsif ($type eq "object_access"){
	$interval = 1800;
	@dses = ("DS:logicalr:ABSOLUTE:3600:U:U", "DS:physr:ABSOLUTE:3600:U:U", "DS:tablescan:ABSOLUTE:3600:U:U");
    } elsif ($type eq "sql_wait"){
	$interval = 1800;
	@dses = (
		 "DS:executions:ABSOLUTE:3600:U:U", 
		 "DS:buffergets:ABSOLUTE:3600:U:U", 
		 "DS:diskr:ABSOLUTE:3600:U:U",
		 "DS:iowait:ABSOLUTE:3600:U:U",
		 "DS:appwait:ABSOLUTE:3600:U:U",
		 "DS:ccwait:ABSOLUTE:3600:U:U",
		 "DS:rows:ABSOLUTE:3600:U:U",
		 "DS:cputime:ABSOLUTE:3600:U:U",
		 "DS:elapsedtime:ABSOLUTE:3600:U:U",
		 "DS:directw:ABSOLUTE:3600:U:U"
		 );
    } elsif ($type eq "table_dml"){
        $interval = 86400;
	@dses = ("DS:inserts:ABSOLUTE:172800:U:U", "DS:updates:ABSOLUTE:172800:U:U", "DS:deletes:ABSOLUTE:172800:U:U");
    } elsif (
	     $type eq "sql_id" 
	     || $type eq "app-sql_id" 
	     || $type eq "waiting-session"
	     ){
	@dses = ("DS:val0:ABSOLUTE:600:U:U", "DS:val1:ABSOLUTE:600:U:U");	
    }

    #RRA:AVERAGE | MIN | MAX | LAST:xff:steps:rows
  
    notify('info', "Creating $RRD with step ${interval} starting at $START");
    if ($type eq "object_access" || $type eq "sql_wait"){ 
      RRDs::create ("$RRD", "-b", $START, "-s", $interval, @dses,
                      "RRA:AVERAGE:0.5:1:43800",
                      "RRA:AVERAGE:0.5:6:600",
                      "RRA:AVERAGE:0.5:24:600",
                      "RRA:AVERAGE:0.5:288:732"
		  );
    } elsif ($type ne "table_dml"){
      RRDs::create ("$RRD", "-b", $START, "-s", $interval, @dses,
                      "RRA:AVERAGE:0.5:1:43800",
                      "RRA:AVERAGE:0.5:6:600",
                      "RRA:AVERAGE:0.5:30:600"
		  );
    } else {
      RRDs::create ("$RRD", "-b", $START, "-s", $interval, @dses,
                      "RRA:AVERAGE:0.5:1:43800",
                      "RRA:MAX:0.5:1:43800",
                      "RRA:AVERAGE:0.5:6:600",
                      "RRA:MAX:0.5:6:600",
                      "RRA:AVERAGE:0.5:24:600",
                      "RRA:MAX:0.5:24:600",
                      "RRA:AVERAGE:0.5:288:732",
                      "RRA:MAX:0.5:288:732"
		  );
    }

    if (my $error = RRDs::error()) {
	return(1, "Cannot create $RRD: $error");
    } else {
	return(0, "$RRD");
    }
}

