#!/usr/bin/perl -w

use strict;
local $| = 1;

require '/ctstats/bin/netappLib.pl';

my($getStatCols);

@{$getStatCols->{'disk'}->{'cols'}} = ("user_reads" ,"user_read_chain" ,"user_read_blocks" ,"user_read_latency" ,"user_writes" ,"user_write_chain" ,"user_write_blocks" ,"user_write_latency" ,"disk_busy");
$getStatCols->{'disk'}->{'type'} = "diskStats";

#disk:*:user_reads disk:*:user_read_chain disk:*:user_read_blocks disk:*:user_read_latency disk:*:user_writes disk:*:user_write_chain disk:*:user_write_blocks disk:*:user_write_latency disk:*:disk_busy

#getStatTable("ro", "10.211.253.21", "volStats", "volume:*:read_data volume:*:read_latency volume:*:read_ops volume:*:write_data volume:*:write_latency volume:*:write_ops volume:*:other_latency volume:*:other_ops", \&printfun);

for my $stat (keys %{$getStatCols}){
    my($stattxt) = "${stat}:*:" . join(" ${stat}:*:", @{$getStatCols->{$stat}->{'cols'}});
    getStatTable("ro", "10.211.253.21", $getStatCols->{$stat}->{'type'}, $stattxt, \&printfun);
}

