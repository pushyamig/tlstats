#!/usr/local/bin/perl -w

use Getopt::Std;
my(%opts);
getopts('D:', \%opts);    #Values in %opts

my($LIB) = ${HOME} . "/lib";
use lib qw($LIB);

use strict;
local $| = 1;
my($DEBUG) = $opts{'D'} || 0;

sub usage () {
    die "Usage: $0 host community\n";
}

use BER;
$BER::pretty_print_timeticks = 0;

use SNMP_util;
## Override the default cache file ("OID_cache.txt") with "cache_test.txt"
#$SNMP_util::CacheFile = "cache_test.txt";
$SNMP_Session::suppress_warnings = 2;
snmpLoad_OID_Cache($SNMP_util::CacheFile);

my($MIBdir) = $LIB . "/mibs";

opendir(DIR, $MIBdir) || die "can't opendir $MIBdir: $!";
my(@files) =readdir(DIR);
closedir DIR;

for my $mib (@files){
    chomp($mib);
    $mib = $MIBdir . "/" . $mib;
    next if ($mib =~ /^\./); #skip dot files
    next unless ($mib =~ /\.mib/); #skip any files that don't have mib in their name
    my($rv) = &snmpQueue_MIB_File($mib);
#  my($rv) = &snmpMIB_to_OID($mib);
    if ($rv && $DEBUG){
	print STDERR "* ERROR: Could not load $mib: $rv\n";
    }
}

my($host) = @ARGV;
chomp($host);
#my($agent) = "public\@${host}:161:2.0:5:1.0:2";
my($agent) = "public\@${host}";


my(@trendoids) = (
		  'miscHighNetRcvdBytes',
		  'miscLowNetRcvdBytes',
		  'tnfsServBadCalls',
		  'miscLowDiskReadBytes',
		  'trpcBadLens',
		  'pclNumber',
		  'diskActiveCount',
		  'cpuUpTime',
		  'miscLowNetSentBytes',
		  'cpuBusyTimePerCent',
		  'miscCacheAge',
		  'miscHighDiskReadBytes',
		  'trpcServXDRCalls',
		  'miscHighNetSentBytes',
		  'diskSpareCount',
		  'miscHighNfsOps',
		  'trpcCalls',
		  'tnfsCalls',
		  'trpcBadCalls',
		  'diskTotalCount',
		  'miscHighDiskWriteBytes',
		  'miscLowDiskWriteBytes',
		  'miscLowNfsOps',
		  'spareNumber',
		  'trpcNullRecvs',
		  );

my(@cols);
sub printfun {
    #shift(@_);
    my(@vals) = @_;
    print "index -> " . shift(@vals) . "\n";
    for (my $i=0;$i <= $#vals; $i++){
	if (! $vals[$i] || $vals[$i] eq ''){
	    $vals[$i] = 0;
	}
	print $cols[$i] . " => " . $vals[$i] . "\n";
    }
    #next if (! @vals);
    #next if (! $vals[0] || ! $vals[1]);
    #print join('|', @vals) . "|\n";
}

my(@smcols) = (
	     "snapmirrorSrc",
	     "snapmirrorDst",
	     "snapmirrorLag",
	     "snapmirrorTotalSuccesses",
	     "snapmirrorTotalRestartSuccesses",
	     "snapmirrorTotalFailures",
	     "snapmirrorTotalDeferments",
	     "snapmirrorTotalTransMBs",
	     "snapmirrorTotalTransTimeSeconds",
	     "snapmirrorThrottleValue",
	     "snapmirrorSyncToAsync",
	      );

my(%trendvars) = (
		  'diskTotalCount' => 'GAUGE',
		  'diskActiveCount' => 'GAUGE',
		  'diskSpareCount' => 'GAUGE',
		  'spareNumber' => 'GAUGE',
		  'pclNumber' => 'GAUGE',
		  'miscCacheAge' => 'GAUGE',
		  'cpuBusyTimePerCent' => 'GAUGE',
		  'cpuUpTime' => 'COUNTER',
		  'trpcCalls' => 'COUNTER',
		  'trpcBadCalls' => 'COUNTER',
		  'trpcNullRecvs' => 'COUNTER',
		  'trpcBadLens' => 'COUNTER',
		  'trpcServXDRCalls' => 'COUNTER',
		  'tnfsCalls' => 'COUNTER',
		  'tnfsServBadCalls' => 'COUNTER',
		  'miscHighNfsOps' => 'COUNTER',
		  'miscLowNfsOps' => 'COUNTER',
		  'miscHighNetRcvdBytes' => 'COUNTER',
		  'miscLowNetRcvdBytes' => 'COUNTER',
		  'miscHighDiskReadBytes' => 'COUNTER',
		  'miscLowDiskReadBytes' => 'COUNTER',
		  'miscHighDiskWriteBytes' => 'COUNTER',
		  'miscLowDiskWriteBytes' => 'COUNTER',
		  'miscHighNetSentBytes' => 'COUNTER',
		  'miscLowNetSentBytes' => 'COUNTER',
	     );

my(%topLevel) = (
                 'miscGlobalStatus' => {
		     'value' => 3,
		     'message' => 'miscGlobalStatusMessage.0'
		     },
                 'envFailedFanCount' => {
                        'value' => 0,
                        'message' => 'envFailedFanMessage.0',
                        },
                 'envFailedPowerSupplyCount' => {
                        'value' => 0,
                        'message' => 'envFailedPowerSupplyMessage.0',
                        },
                 'fsOverallStatus' => {
                        'value' => 1,
                        'message' => 'fsStatusMessage.0',
                        },
                'diskFailedCount' => {
                        'value' => 0,
                        'message' => 'diskFailedMessage.0',
                        },
);
my(@tloids) = keys %topLevel;

my(%levelChecks);
$levelChecks{'default'}{'spareNumber'} = 0;
$levelChecks{'default'}{'nfsIsLicensed'} = 2;
$levelChecks{'default'}{'envOverTemperature'} = 1;
$levelChecks{'default'}{'nvramBatteryStatus'} = 1; # 'ok'
$levelChecks{'default'}{'cpuCount'} = 2;
$levelChecks{'default'}{'miscCorrectedMachineChecks'} = 0;
$levelChecks{'default'}{'ndmpOn'} = 1;
$levelChecks{'default'}{'diskReconstructingCount'} = 0;
$levelChecks{'default'}{'diskReconstructingParityCount'} = 0;
$levelChecks{'default'}{'diskVerifyingParityCount'} = 0;
$levelChecks{'default'}{'diskScrubbingCount'} = 0;
$levelChecks{'default'}{'diskAddingSpareCount'} = 0;
$levelChecks{'default'}{'snapmirrorOn'} = 2;

my(@lcoids) = keys %{$levelChecks{'default'}};
#my @cols = keys %trendvars;

 my @sisoids2 = (
	       "sisPath",
	       "sisState",
	       "sisStatus",
	       "sisProgress",
	       "sisType",
	       "sisLastOpBeginTime",
	       "sisLastOpEndTime",
	       "sisHighLastOpSize",
	       "sisLowLastOpSize",
	       "sisLastOpError",
	       "sis64LastOpSize",
	       );

my(@sisoids) = (
		"sisPath",
		"sisHighLastOpSize",
		"sisLowLastOpSize",
		"sis64LastOpSize",
	       );


#my @cols = @trendoids;
@cols = @sisoids2;
print <<DE;
DEBUG: ${DEBUG}
DE
my($nrows) = &snmpmaptable($agent, \&printfun, @cols);

#my($nrows) = &snmpmaptable($agent, \&printfun, { 'use_getbulk' => 0 }, @cols);

#my($nrows) = &snmpmaptable($agent, \&printfun, @tloids);
print $nrows . "\n";
#$nrows = &snmpmaptable($agent, \&printfun, @lcoids);
#print $nrows;

