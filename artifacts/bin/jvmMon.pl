#!/usr/bin/perl -w
#
#
## TTD:
# - test new code layout
# - add hrStorage

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/jvmMon.pl $
# $Id: jvmMon.pl 2221 2015-12-16 15:38:44Z pushyami $
#
# Copyright (C) 2000-2007 by R.P. Aditya <aditya@grot.org>
# (See "License", below.)
#
# script to retrieve stats about Sun JVM
# and stuff them into an RRD
#
# License:
#    This program is free software; you can redistribute it and/or
#    modify it under the terms of the GNU General Public License
#    as published by the Free Software Foundation; either version 2
#    of the License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You may have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
#    USA.
#
#    An on-line copy of the GNU General Public License can be found
#    http://www.fsf.org/copyleft/gpl.html.
#

use strict;
local $| = 1;

use RRDs;

use FindBin;
my $homeDir = ${FindBin::Bin}."/..";

require "$homeDir/bin/monitor.pl";

my($config);
$config->{'STEP'} = 60;
$config->{'DEBUG'} = 0;
$config->{'program'} = 'jvmMon';
$config->{'version'} = "0.01";

$config->{'homeDir'} = $homeDir;

$config->{'libDir'} = $config->{'homeDir'} . "/lib";
$config->{'mibDir'} = $config->{'libDir'} . "/mibs";

our $sharedDataDir;

$config->{'dataDir'} = "$sharedDataDir/jvm";

$config->{'timeout'} = 15;
if (! defined $config->{'logfacility'}){
  $config->{'logfacility'} = 'user';
}

openlog($config->{'program'},'cons,pid', $config->{'logfacility'});

use lib qw($config->{'libDir'});

use SNMP_util;
snmpLoad_OID_Cache($SNMP_util::CacheFile);
$SNMP_Session::suppress_warnings = 2;

opendir(DIR, $config->{'mibDir'}) || die "can't opendir $config->{'mibDir'}: $!";
my(@files) =readdir(DIR);
closedir DIR;

for my $mib (@files){
    chomp($mib);
    $mib = $config->{'mibDir'} . "/" . $mib;
    next if ($mib =~ /^\./); #skip dot files
    next unless ($mib =~ /\.mib/); #skip any files that don't have mib in their name
    my($rv) = &snmpQueue_MIB_File($mib);
    if (! $rv && $config->{'DEBUG'}){
	print STDERR "* ERROR: Could not load $mib: $rv\n";
    }
}

my($hostname);
my(@mempoolcols) = (
		    "jvmMemPoolName",
		    "jvmMemPoolInitSize",
		    "jvmMemPoolUsed",
		    "jvmMemPoolCommitted",
		    "jvmMemPoolCollectUsed",
		    "jvmMemPoolCollectCommitted",
	     );

sub mempoolupdate {
    update('mempool', @_);
}

my(@memmgrcols) = (
		   "jvmMemManagerName",
		   "jvmMemGCCount",
		   "jvmMemGCTimeMs",
		   );
sub memmgrupdate {
    update('memmgr', @_);
}

my(@jvmaggcols) = (
	     "jvmClassesTotalLoadedCount",
	     "jvmClassesUnloadedCount",
	     "jvmMemoryHeapInitSize",
	     "jvmMemoryHeapUsed",
	     "jvmMemoryHeapCommitted",
	     "jvmMemoryNonHeapInitSize",
	     "jvmMemoryNonHeapUsed",
	     "jvmMemoryNonHeapCommitted",
	     "jvmThreadCount",
	     "jvmThreadDaemonCount",
	     "jvmThreadTotalStartedCount",
	     "jvmRTUptimeMs",
	     "jvmJITCompilerTimeMs",
	       );
sub jvmaggupdate {
    update('jvmagg', @_);
}

my(@threadcols) = (
             "jvmThreadInstName",
             "jvmThreadInstCpuTimeNs",
             "jvmThreadInstWaitTimeMs",
             "jvmThreadInstWaitCount",
             "jvmThreadInstBlockTimeMs",
             "jvmThreadInstBlockCount",
		   );

sub threadupdate {
    update('thread', @_);
}

my(@syscpucols) = (
		"ssCpuRawUser",
		"ssCpuRawNice",
		"ssCpuRawSystem",
		"ssCpuRawIdle",
		"ssCpuRawWait",
		"ssCpuRawKernel",
		"ssRawInterrupts",
		"ssRawContexts",
		);

sub syscpuupdate {
    update('syscpu', @_);
}

my(@sysmemcols) = (
		   "memAvailSwap",
		   "memTotalSwap",
		   "memAvailReal",
		   "memTotalReal",
		   "memShared",
		   "memBuffer",
		   "memCached",
		   );
sub sysmemupdate {
    update('sysmem', @_);
}

my(@dskcols) = (
              "dskPath",
              "dskDevice",
              "dskAvail",
              "dskUsed",
              "dskPercentNode",
		  );
sub dskupdate {
    update('dsk', @_);
}

my(@diskiocols) = (
		   "diskIODevice",
		   "diskIONRead",
		   "diskIONWritten",
		   "diskIOReads",
		   "diskIOWrites",
		   );

sub diskioupdate {
    update('diskio', @_);
}

my(@tcpAllconncols) = (
        "tcpAllConnections",
);

my(@tcpEstabconncols) = (
        "tcpEstabConnections",
);

my(@tcpTWconncols) = (
        "tcpTWConnections",
);

sub tcpEstablishedconnupdate {
        update("nsEstablishedExtensions", @_);
}

sub tcpAllconnupdate {
        update("nsAllExtensions", @_);
}

sub tcpTWconnupdate {
        update("nsTWExtensions", @_);
}


my(@hrstoragecols) = (
 #		      "hrStorageType",
		      "hrStorageDescr",
		      "hrStorageAllocationUnits",
		      "hrStorageSize",
		      "hrStorageUsed",
		      "hrStorageAllocationFailures",
		   );

sub hrstorageupdate {
    update('hrstorage', @_);
}

## add these
# hrStorageIndex                                hrStorageType    hrStorageDescr hrStorageAllocationUnits hrStorageSize hrStorageUsed hrStorageAllocationFailures
# add hrStore, hrStoreCols, 
          


sub update {
# Take type and args to update rrd file
# Some special processing may be done based on type.

    my($type) = shift(@_);
    my($first) = shift(@_);

    my($RRD);
    my($STEP) = $config->{'STEP'};

    if ($type eq "mempool" || $type eq "memmgr") {
      my($name) = shift(@_);
      $name =~ s/\s+/_/g;
      $RRD = $config->{'dataDir'} . "/" . $type . "/" . $hostname . "." . $first . "_" . $name . ".rrd";
    }

    if ($type eq "jvmagg") {
      $RRD = $config->{'dataDir'} . "/" . $type . "/" . $hostname . ".rrd";
    }

    if ($type eq "syscpu" || $type eq "sysmem") {
      $RRD = $config->{'dataDir'} . "/system/" . $type . "/" . $hostname . ".rrd";
    }

    if ($type eq "dsk") {
      my($name) = shift(@_);
      $name = $name . "-" . shift(@_);
      $name =~ s/\//_/g;	
      $RRD = $config->{'dataDir'} . "/system/" . $type . "/" . $hostname . "." . $name . ".rrd";
    }

    if ($type eq "diskio") {
      my($name) = shift(@_);
      $name =~ s/\//_/g;	
      $RRD = $config->{'dataDir'} . "/system/" . $type . "/" . $hostname . "." . $name . ".rrd";
    }

    if ($type eq "nsAllExtensions") {
       $RRD = $config->{'dataDir'} . "/" . "nsExtensions/tcp/" . $hostname . "-all"  . ".rrd";
    }

    if ($type eq "nsEstablishedExtensions") {
       $RRD = $config->{'dataDir'} . "/" . "nsExtensions/tcp/" . $hostname . "-estab"  . ".rrd";
    }

    if ($type eq "nsTWExtensions") {
        $RRD = $config->{'dataDir'} . "/" . "nsExtensions/tcp/" . $hostname . "-tw"  . ".rrd";
    }
    
   if ($type eq "hrstorage") {
      my($name) = shift(@_);
      $name =~ s/\//_/g;	
      $RRD = $config->{'dataDir'} . "/system/" . $type . "/" . $hostname . "." . $name . ".rrd";
    }

    if ($type eq "thread") {
      my($name) = shift(@_);
      $name =~ s/\s+/_/g;
      my($threaddir) = $config->{'dataDir'} . "/" . $type . "/" . $hostname;
      if (! -d "${threaddir}") {
	if (! mkdir "${threaddir}", 0750) {
	  notify('crit', "Could not create dir ${threaddir} ($!), skipping");
	  next;
	}
      }
      $RRD = $threaddir . "/" . $name . "." . $first . ".rrd";
    }

    my(@vals) = @_;

    my($t) = time;
    $t = int($t);

    for (my $i=0;$i<=$#vals;$i++){
	if (! $vals[$i] || $vals[$i] eq ''){
	    $vals[$i] = 0;
	}
	my $timeval = $vals[$i];
	chomp($timeval);
	if ($timeval =~ /\d*\:\d+\:\d+/){
	    $timeval =~ s/(\d*)\:(\d+)\:(\d+)//;
	    my ($hrs, $mins, $secs) = ($1, $2, $3);
	    $vals[$i] = $hrs * 3600 + $mins * 60 + $secs;
	}
    }

    if (! -e $RRD){
	my($START) = time - (2 * $STEP);
  
	if (! ensureRRDDirectory($RRD)) {
	  notify('crit', "Could not create ${RRD} ($!), skipping");
	}

	notify('info', "Creating $RRD with step ${STEP} starting at $START");
	my($v, $msg) = RRD_create($RRD, $START, $STEP, $type);
	if ($v){
        notify('err', "couldn't create $RRD because $msg");
        return;
      } 
    } 

    my($rv, $errmsg) = updateRRD($RRD, $t, @vals);
    if ($rv){
	notify('err', "error updating $RRD : ${errmsg}");
    }
}

sub RRD_create {
    my($RRD, $START, $interval, $type) = @_;
    my(@dses);
    my(@rras) = (
		 "RRA:AVERAGE:0.5:1:3000",
		 "RRA:MAX:0.5:1:3000",
		 "RRA:AVERAGE:0.5:5:3000",
		 "RRA:MAX:0.5:5:3000",
		 "RRA:AVERAGE:0.5:10:5000",
		 "RRA:MAX:0.5:10:5000",
		 "RRA:AVERAGE:0.5:1440:732",
		 "RRA:MAX:0.5:1440:732"
 		 );

    if ($type eq "mempool"){
	@dses = (
		    "DS:mempoolisz:GAUGE:1200:U:U",
		    "DS:mempoolused:GAUGE:1200:U:U",
		    "DS:mempoolcomm:GAUGE:1200:U:U",
		    "DS:mempoolcollu:GAUGE:1200:U:U",
		    "DS:mempoolcollcomm:GAUGE:1200:U:U",
	     );
    }
    if ($type eq "memmgr"){
	@dses = (
		 "DS:memmgrgc:COUNTER:1200:U:U",
		 "DS:memmgrgctms:COUNTER:1200:U:U",
		 );
    }
    if ($type eq "thread"){
	@dses = (
             "DS:cputimens:COUNTER:1200:U:U",
             "DS:waittimems:COUNTER:1200:U:U",
             "DS:waitcount:COUNTER:1200:U:U",
             "DS:blocktimems:COUNTER:1200:U:U",
             "DS:blockcount:COUNTER:1200:U:U",
		 );
    }
    if ($type eq "syscpu"){
	@dses = (
		"DS:cpuuser:COUNTER:1200:U:U",
		"DS:cpunice:COUNTER:1200:U:U",
		"DS:cpusystem:COUNTER:1200:U:U",
		"DS:cpuidle:COUNTER:1200:U:U",
		"DS:cpuwait:COUNTER:1200:U:U",
		"DS:cpukernel:COUNTER:1200:U:U",
		"DS:interrupts:COUNTER:1200:U:U",
		"DS:contexts:COUNTER:1200:U:U",
		);
    }
    if ($type eq "dsk"){
	@dses = (
              "DS:avail:GAUGE:1200:U:U",
              "DS:used:GAUGE:1200:U:U",
              "DS:nodepercent:GAUGE:1200:U:U",
		 );
    }
    if ($type eq "diskio"){
	@dses = (
              "DS:read:COUNTER:1200:U:U",
              "DS:written:COUNTER:1200:U:U",
              "DS:reads:COUNTER:1200:U:U",
              "DS:writes:COUNTER:1200:U:U",
		 );
    }


   if ($type eq "nsAllExtensions"){
         @dses = (
                  "DS:tcpAllConnections:GAUGE:1200:U:U",
        );
    }

    if ($type eq "nsEstablishedExtensions") {
        @dses = (
                "DS:tcpEstabConnections:GAUGE:1200:U:U",
        );
    }

    if ($type eq "nsTWExtensions") {
        @dses = (
                "DS:tcpTWConnections:GAUGE:1200:U:U",
        );
    }

 #dsss  fix up the update call to get the right values maybe add computed to deal with varying allocation unit sizes?

    if ($type eq "hrstorage"){
	@dses = (
              "DS:allocationunits:GAUGE:1200:U:U",
              "DS:size:GAUGE:1200:U:U",
              "DS:used:GAUGE:1200:U:U",
              "DS:allocationfailures:COUNTER:1200:U:U",
		 );
    }
    if ($type eq "sysmem"){
	@dses = (

		   "DS:memtotalswap:GAUGE:1200:U:U",
		   "DS:memavailreal:GAUGE:1200:U:U",
		   "DS:memtotalreal:GAUGE:1200:U:U",
		   "DS:memshared:GAUGE:1200:U:U",
		   "DS:membuffer:GAUGE:1200:U:U",
		   "DS:memcached:GAUGE:1200:U:U",
		   );
    }
    if ($type eq "jvmagg"){
	@dses = (
		 "DS:classesloaded:ABSOLUTE:1200:U:U",
		 "DS:classesunloaded:ABSOLUTE:1200:U:U",
		 "DS:memheapinit:GAUGE:1200:U:U",
		 "DS:memheapused:GAUGE:1200:U:U",
		 "DS:memheapcomm:GAUGE:1200:U:U",
		 "DS:memnonheapinit:GAUGE:1200:U:U",
		 "DS:memnonheapused:GAUGE:1200:U:U",
		 "DS:memnonheapcomm:GAUGE:1200:U:U",
		 "DS:threadcount:GAUGE:1200:U:U",
		 "DS:threaddaemoncount:GAUGE:1200:U:U",
		 "DS:threadtotstarted:COUNTER:1200:U:U",
		 "DS:rtuptimems:COUNTER:1200:U:U",
		 "DS:jitcompilertms:COUNTER:1200:U:U",
		 );
    }
    ## experimental, check to see if dses were set
    if (!@dses) {
	notify('ERR', "could not create RRD of type ${type}");
	return(1, "do not recognize type ${type}");
    }


    RRDs::create ("$RRD", "-b", $START, "-s", $interval, @dses, @rras);

    if (my $error = RRDs::error()) {
	return(1, "Cannot create $RRD: $error");
    } else {
	return(0, "$RRD");
    }
}


while(<>){
    chomp;
    next if (/(^$)|(^#)/);
    my($community, $ip);
    ($hostname, $community, $ip) = split(/\|/);
    my($agent) = $community . "\@" . $ip;

    my(%sets);

    $sets{"mempoolcols"}{"cols"} = \@mempoolcols;
    $sets{"mempoolcols"}{"func"} = \&mempoolupdate;
    $sets{"mempoolcols"}{"exclude"} = '^(lr|db|tlstats)\.';

    $sets{"memmgrcols"}{"cols"} = \@memmgrcols;
    $sets{"memmgrcols"}{"func"} = \&memmgrupdate;
    $sets{"memmgrcols"}{"exclude"} = '^(lr|db|tlstats)\.';

    $sets{"jvmaggcols"}{"cols"} = \@jvmaggcols;
    $sets{"jvmaggcols"}{"func"} = \&jvmaggupdate;
    $sets{"jvmaggcols"}{"exclude"} = '^(lr|db|tlstats)\.';

    $sets{"threadcols"}{"cols"} = \@threadcols;
    $sets{"threadcols"}{"func"} = \&threadupdate;
    $sets{"threadcols"}{"exclude"} = '.*';

    $sets{"syscpucols"}{"cols"} = \@syscpucols;
    $sets{"syscpucols"}{"func"} = \&syscpuupdate;

    $sets{"sysmemcols"}{"cols"} = \@sysmemcols;
    $sets{"sysmemcols"}{"func"} = \&sysmemupdate;

    $sets{"tcpAllconncols"}{"cols"} = \@tcpAllconncols;
    $sets{"tcpAllconncols"}{"func"} = \&tcpAllconnupdate;

    $sets{"tcpEstabconncols"}{"cols"} = \@tcpEstabconncols;
    $sets{"tcpEstabconncols"}{"func"} = \&tcpEstablishedconnupdate;

    $sets{"tcpTWconncols"}{"cols"} = \@tcpTWconncols;
    $sets{"tcpTWconncols"}{"func"} = \&tcpTWconnupdate;

    $sets{"diskiocols"}{"cols"} = \@diskiocols;
    $sets{"diskiocols"}{"func"} = \&diskioupdate;

    $sets{"dskcols"}{"cols"} = \@dskcols;
    $sets{"dskcols"}{"func"} = \&dskupdate;
    $sets{"dskcols"}{"exclude"} = '^db\.';

    $sets{"hrstoragecols"}{"cols"} = \@hrstoragecols;
    $sets{"hrstoragecols"}{"func"} = \&hrstorageupdate;
#    $sets{"hrstoragecols"}{"exclude"} = '^db\.';
    my($timeData) = localtime();
    notify('debug', "starting collection for ${hostname} at $timeData");
    for my $set (keys %sets){
	if (defined ${sets{$set}{"exclude"}} ){
    	   next if ($hostname =~ /${sets{$set}{"exclude"}}/);
        }
        notify('debug', "starting ${set} collection for ${hostname}");
	my($rv) = &snmpmaptable($agent, $sets{$set}{"func"}, { 'use_getbulk' => 0 }, @{$sets{$set}{"cols"}});
        if (!defined($rv) || $rv < 0){
   	  notify('err', "getting ${set} from ${hostname}");
        } else {
          notify('debug', "collected ${rv} rows in ${set} collection for ${hostname}");
        }
    }
    notify('debug', "ending collection for ${hostname}");
}

