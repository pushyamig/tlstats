#!/bin/bash
# Take xml file and restore the corresponding rrd file

# $HeadURL: https://wush.net/svn/ctools/ctstats/data-collection/trunk/bin/rrdReload.sh $
# $Id: rrdReload.sh 1564 2012-03-15 18:27:52Z dlhaines $

set -u 
set -e

# Get a file names from command line 
# restore the rrd file

for file in "$@"
do 
    rrdFile=${file%.xml}
    echo "restoring file: [$rrdFile]";
    if [ -e "$rrdFile" ]; then
	mv "$rrdFile" "$rrdFile.bkp";
    fi
    rrdtool restore "$file" "$rrdFile";
done

#end
