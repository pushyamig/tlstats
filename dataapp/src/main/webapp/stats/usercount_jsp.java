package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import java.sql.*;
import oracle.jdbc.driver.*;

public final class usercount_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(4);
    _jspx_dependants.add("/header.jsp");
    _jspx_dependants.add("/common/dbheader.jsp");
    _jspx_dependants.add("/common/dbfooter.jsp");
    _jspx_dependants.add("/footer.jsp");
  }

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;


        long start = System.currentTimeMillis();


        DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
	String dbhost = request.getParameter("h");
	String dbport = request.getParameter("p");
	String dbname = request.getParameter("n");

	if (dbhost != null && !dbhost.equals("")){
	} else {
		dbhost = "ctoolsdbcluster.ds.itd.umich.edu";
	}
	if (dbport != null && !dbport.equals("")){
	} else {
		dbport = "1521";
	}
	String dbuser = request.getParameter("u");
	if (dbuser != null && !dbuser.equals("")){
	} else {
		dbuser = "db_monitor";
	}
	String dbpwd = request.getParameter("w");
	if (dbpwd != null && !dbpwd.equals("")){
	} else {
		dbpwd = "XXXXXXX";
	}

	if (dbname != null && !dbname.equals("")){
	} else {
		dbname = "CTOOLS";
	}

	String jdbcurl = "jdbc:oracle:thin:" + dbuser + "/" + dbpwd + "@" + dbhost + ":" + dbport + ":" + dbname;

        Connection conn = DriverManager.getConnection(jdbcurl);


        response.setContentType("text/plain");

        Statement stmt = conn.createStatement();
        ResultSet rset = stmt.executeQuery("select session_server, count(distinct(session_user)) as num_users, count(session_id) as num_sessions from sakai_session where session_start >= SYSDATE - 1 and session_start = session_end group by session_server order by session_server");
        while (rset.next()){
              String server = (rset.getString(1)).replaceAll("-.*", "");
	      String users = rset.getString(2);
	      String sessions = rset.getString(3);

      out.print(server);
      out.write('|');
      out.print(users);
      out.write('|');
      out.print(sessions);
      out.write('|');
      out.write('\n');

        }
	rset.close();
        stmt.close();


	conn.close();


        long end = System.currentTimeMillis();
        long totaltime = end - start;

      out.write("this script runtime|");
      out.print(totaltime);
      out.write('|');
      out.write('\n');
      out.write('\n');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
