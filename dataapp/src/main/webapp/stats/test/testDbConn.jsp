<%-- Test db connection --%>
<%@ include file="../common/getProperties.jsp" 
%><%@ include file="../common/header.jsp"
%><%@ include file="../common/dbheader.jsp"
%><%-- deal with account information 
--%><%
//String user = properties.getProperty("user");
//String pwd = properties.getProperty("password");


if (dbuser == null || dbpwd == null) {
	response.setStatus(javax.servlet.http.HttpServletResponse.SC_FORBIDDEN);
	return;
}

	if (trace > 0) {
		System.out.println("dbuser: "+dbuser);
		%>
		<p/>dbuser: <%=dbuser%><p/>
		<%
	}
    response.setContentType("text/plain");

	String q1 = "select 'dbmetric', metric_name, value, 0 from v$sysmetric";

%>
<%-- 
	String q2 = "select 'tblsp', f.tablespace_name as one, (sum(f.BYTES) - sum(s.bytes)) as two, 0 from dba_data_files f, dba_free_space s where f.tablespace_name = s.tablespace_name and f.tablespace_name not like 'UNDO%' and f.tablespace_name not like 'SYS%' group by f.tablespace_name";

	String q3 = "select 'table', owner || '.' || table_name, num_rows, case when blocks is null then 0 else blocks end from dba_tables where owner not in ('SYS', 'SYSMAN', 'MDSYS', 'PERFSTAT', 'XDB', 'TSMSYS', 'EXFSYS', 'WMSYS', 'ORDSYS', 'OUTLN', 'DBSNMP', 'SYSTEM', 'CTXSYS') and num_rows > 0";

	String q4 = "select 'session', s.username || '.' || s.machine, (select count(*) from v$session where machine = s.machine and status = 'ACTIVE'), (select count(*) from v$session where machine = s.machine and status = 'INACTIVE') from v$session s group by s.username, s.machine";

	String q5 = "select 'active-session', s.username || '.' || s.machine, count(*), 0 from v$active_session_history a, v$session s where a.sample_time >= (sysdate - interval '5' minute(1)) and a.session_id = s.sid and a.session_type = 'FOREGROUND' and a.session_state = 'ON CPU' group by s.username, s.machine, a.session_type, a.session_state";

	String q6 = "select 'waiting-session', s.username || '.' || s.machine, count(*), 0 from v$active_session_history a, v$session s where a.sample_time >= (sysdate - interval '5' minute(1)) and a.session_id = s.sid and a.session_type = 'FOREGROUND' and a.session_state = 'WAITING' group by s.username, s.machine, a.session_type, a.session_state";

	String q7 = "select 'sql_id', case when a.sql_id is null then 'null' else a.sql_id end as sql_id, count(*), 0 as count from v$active_session_history a where a.sample_time >= (sysdate - interval '5' minute(1)) group by a.sql_id having count(*) > 300";

	// per app-server most run
	String q8 = "select 'app-sql_id', s.username || '.' || s.machine || '-' || case when a.sql_id is null then 'null' else a.sql_id end as sql_id, count(*), sum(a.wait_time) from v$active_session_history a, v$session s where a.sample_time >= (sysdate - interval '5' minute(1)) and a.session_id = s.sid group by s.username, s.machine, a.sql_id having count(*) > 100";

	// per app-server most run and most-waiting sqls 
	String q9 = "select 'app-sql_id', s.username || '.' || s.machine || '-' || a.sql_id as sql_id, count(*), sum(a.wait_time) from v$active_session_history a, v$session s where a.sample_time >= (sysdate - interval '5' minute(1)) and a.session_id = s.sid and a.sql_id is not null group by s.username, s.machine, a.sql_id having count(*) > 300 or (sum(a.wait_time) > 300000 and count(*) > 2)";

	// session memory by machine and user
	String q10 = "select 'session-mem', vses.username || '.' || vses.machine, count(*) as val0, sum(vsst.value) as val1 from v$sesstat vsst, v$statname vstt, v$session vses where vstt.statistic# = vsst.statistic# and vsst.sid = vses.sid and vstt.name ='session pga memory' group by vses.machine, vses.username";

	//object access by db object (tables and indices)
	String q11 = "select 'object_access', o.owner || '.' || o.object_name || '.' || o.object_type, cast (((cast (sys_extract_utc(end_interval_time) as date) - to_date('1970-01-01 00:00:00','yyyy-mm-ddhh24:mi:ss')) * 86400) as integer) as tstamp, sum(logical_reads_total), sum(physical_reads_total), sum(table_scans_total) from dba_hist_seg_stat s, dba_hist_seg_stat_obj o, dba_hist_snapshot sn where s.obj# = o.obj# and sn.snap_id = (select max(snap_id) from dba_hist_snapshot) and sn.snap_id = s.snap_id and o.owner not in ('SYS', 'SYSTEM', 'SYSMAN', 'MDSYS', 'DBSNMP') group by o.owner, o.object_name, o.object_type, end_interval_time, logical_reads_total, physical_reads_total, table_scans_total";

	//table dml
	String q12 = "select 'table_dml', table_owner || '.' || table_name, cast (((cast(sys_extract_utc(cast(max(timestamp) as timestamp)) as date) - to_date('1970-01-01 00:00','yyyy-mm-ddhh24:mi:ss')) * 86400) as integer) as tstamp, inserts, updates, deletes from all_tab_modifications where table_owner not in ('SYS', 'SYSTEM', 'SYSMAN', 'MDSYS', 'DBSNMP') group by table_owner, table_name, updates, inserts, deletes";

	//session stats
	String t = "select 'sesstat',  n.name as stat, s.username || '.' || s.machine, count(*) as sessions, sum(t.value) as value from v$sesstat t, v$session s, v$statname n where t.sid = s.sid and t.statistic# = n.statistic# and t.value != 0 group by s.machine, s.username, n.name order by s.username, s.machine, n.name";

	String u = "select 'eventwait',  wait_class || '.' || event, total_waits, time_waited_micro, total_timeouts from v$system_event order by time_waited_micro desc";

	// sql waits
	// see: http://www.dba-oracle.com/t_time_series_sql_performance_tracking.htm
	// and: http://www.sc.ehu.es/siwebso/KZCC/Oracle_10g_Documentacion/server.101/b10755/statviews_2158.htm
	//
	String v = "select 'sql_wait', sql.sql_id, cast (((cast (sys_extract_utc(sn.end_interval_time) as date) - to_date('1970-01-01 00:00:00','yyyy-mm-ddhh24:mi:ss')) * 86400) as integer) as tstamp, sum(sql.executions_delta), sum(sql.buffer_gets_delta), sum(sql.disk_reads_delta), sum(sql.iowait_delta), sum(sql.apwait_delta), sum(sql.ccwait_delta), sum(sql.rows_processed_delta), sum(sql.cpu_time_delta), sum(sql.elapsed_time_delta), sum(sql.direct_writes_delta) from dba_hist_sqlstat sql, dba_hist_snapshot sn where sn.snap_id = (select max(snap_id) from dba_hist_snapshot) and sn.snap_id = sql.snap_id and rownum <= 100 group by sql.sql_id, sql.rows_processed_delta, sql.cpu_time_delta, sql.elapsed_time_delta, sql.direct_writes_delta, sql.executions_delta, sql.buffer_gets_delta, sql.disk_reads_delta, sql.iowait_delta, sql.apwait_delta, sql.ccwait_delta, sn.end_interval_time, sn.begin_interval_time order by sql.elapsed_time_delta desc, sql.iowait_delta desc";

	String w = "select 'px_stat', statistic, value from v$pq_sysstat";
--%>
<%
	Vector rsets = new Vector();

	rsets.addElement(q1);
	%>
	<%--
	rsets.addElement(q2);
	rsets.addElement(q3);
	rsets.addElement(q4);
	rsets.addElement(q5);
	rsets.addElement(q6);
	rsets.addElement(q9);
	rsets.addElement(q10);
	rsets.addElement(q11);
	rsets.addElement(q12);
	rsets.addElement(t);
	rsets.addElement(u);
	rsets.addElement(v);
	rsets.addElement(w);

	if (
		dbname.equals("CTOOLS")
	//	|| dbname.equals("CTLOAD") 
	//	|| dbname.equals("CTTEST") 
	){
		//rsets.addElement(u);
	}
--%>
<%
        Statement stmt = conn.createStatement();

	for (Enumeration e = rsets.elements(); e.hasMoreElements();){
		String qr = (String) e.nextElement();
		try {
		        ResultSet rset = stmt.executeQuery(qr);
			ResultSetMetaData rsmd = rset.getMetaData();
		     	int numberOfColumns = rsmd.getColumnCount();
		        while (rset.next()){
				for ( int i = 1; i <= numberOfColumns; i++){ 
		                	String val = rset.getString(i);
%><%=val%>|<%
				}
%>
<%
		        }
			rset.close();
		} catch (Exception ex){
			response.setStatus(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			%>
<%=ex.toString()%> - <%=qr%><%		
		}
	 finally {
        stmt.close();
	}
	}
%>

<%@ include file="../common/dbfooter.jsp"%>
<%@ include file="../common/footer.jsp"%>
