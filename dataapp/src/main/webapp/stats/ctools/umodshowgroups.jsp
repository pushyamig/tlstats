<%@ page language="java" import="java.util.*,java.sql.*,javax.naming.*,javax.naming.directory.*" 
%><%
	String u = request.getParameter("u");
	String g = showMembership(u);
%>
<%=g%>
<%!
private String showMembership(String un) throws Exception {	
	Hashtable env = new Hashtable();
	env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
	env.put(Context.PROVIDER_URL, "ldap://ldap.itd.umich.edu:389/dc=umich,dc=edu");

	try {
	    javax.naming.directory.DirContext ctx = new javax.naming.directory.InitialDirContext(env);
	    String[] attrIDs = {"cn"};
	    javax.naming.directory.SearchControls ctls = new javax.naming.directory.SearchControls();
	    String filter = "(&(member=uid=" + un + ",ou=People,dc=umich,dc=edu)(objectclass=rfc822mailgroup))";
	    ctls.setReturningAttributes(attrIDs);
	    ctls.setReturningObjFlag(true);
	    ctls.setSearchScope(javax.naming.directory.SearchControls.SUBTREE_SCOPE);
	    String searchBase = "";
	    javax.naming.NamingEnumeration s = ctx.search(searchBase, filter, ctls);
	    String results = "";
	    while (s.hasMore()) {
		SearchResult sr = (SearchResult)s.next();
		javax.naming.NamingEnumeration e = (sr.getAttributes()).getAll();
		while (e.hasMoreElements()){
			javax.naming.directory.Attribute attr = (javax.naming.directory.Attribute) e.nextElement();
			javax.naming.NamingEnumeration a = attr.getAll();
			while (a.hasMoreElements()){
				String val = (String) a.nextElement();
				results = results + "'" + val + "', ";
			}
			a.close();
		}
		e.close();
	    }
	    s.close();
	    ctx.close();
	    results = results + "'null'";
	    return results;
	} catch (javax.naming.NamingException e) {
	    System.err.println("Problem getting attribute:" + e);
	    return "";
	}
}
%>