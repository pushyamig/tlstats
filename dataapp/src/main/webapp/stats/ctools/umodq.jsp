<%@ page language="java" import="java.util.*,java.sql.*,oracle.jdbc.driver.*,javax.naming.*" 
%><%
	String un = request.getParameter("u");
	Hashtable env = new Hashtable();
	env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
	env.put(Context.PROVIDER_URL, "ldap://ldap.itd.umich.edu:389/dc=umich,dc=edu");

try {

    // Create the initial directory context
    javax.naming.directory.DirContext ctx = new javax.naming.directory.InitialDirContext(env);

	    
    // Ask for all attributes of the object 
//    String[] attrIDs = {"ou", "title"};
    String[] attrIDs = {"ou"};
//    attrIDs = null;
    String umodq = "uid=" + un + ",ou=People";
    javax.naming.directory.Attributes attrs = ctx.getAttributes(umodq, attrIDs);
    javax.naming.NamingEnumeration e = attrs.getAll();
    while (e.hasMoreElements()){
	javax.naming.directory.Attribute attr = (javax.naming.directory.Attribute) e.nextElement();
	String id = attr.getID();
	%><%=un%> <%=id%>: <%
	javax.naming.NamingEnumeration a = attr.getAll();
	while (a.hasMoreElements()){
		String val = (String) a.nextElement();
		%><%=val%>|<%
	}
	a.close();
   }
   %><br/><%
   e.close();
   ctx.close();
} catch (javax.naming.NamingException e) {
    System.err.println("Problem getting attribute:" + e);
}

%>