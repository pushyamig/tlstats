<%@ page import="java.util.Enumeration" %>
<html>
<head>
<title>JSP Request Headers</title>
</head>
<body>
<h1>JSP Request Headers</h1>
Request URI = <code><%= request.getRequestURI () %></code>
<ul>
	<li>getRemoteAddr: <%=request.getRemoteAddr()%></li>
	<li>getRemoteHost: <%=request.getRemoteHost()%></li>
</ul>
<p>
<%
    Enumeration headers = request.getHeaderNames();
    Enumeration parameters = request.getParameterNames();
    if (headers == null)
	%>No Request Headers<%
    else {
        String headername;
        String headerval;
	%><h4>Request Headers</h4><ul><%
        while (headers.hasMoreElements ()){
            headername = (String) headers.nextElement ();
            headerval = request.getHeader (headername);
%>
            <li><code><%= headername %></code> =
<%
            if (headerval == null)
              {
%>
                &lt;null&gt;
<%
	      }
            else
              {
%>
                <code><%= headerval %></code>
<%
	      }
	  }
%>
        </ul>
<%
    }
    if (parameters == null)
	%>No Request Parameters<%
    else
      {
        String parametername;
	String parameterval;
%>
        <h4>Request Parameters</h4><ul>
<%
        while (parameters.hasMoreElements ())
          {
            parametername = (String) parameters.nextElement ();
            String[] parametervals = request.getParameterValues (parametername);
		for (int i=0;i<parametervals.length;i++){
%>
            <li><code><%= parametername %></code> =
<%
            if (parametervals[i] == null)
              {
%>
                &lt;null&gt;
<%
	      }
            else
              {
%>
                <code><%= parametervals[i] %></code>
<%
	      }
		}
	  }
%>
        </ul>
<%
      }
%>
<hr/>
Thanks to: <a href="http://agnes.dida.physik.uni-essen.de/~elug/themenabende/tomcat/jsp/showhdrs.html">http://agnes.dida.physik.uni-essen.de/~elug/themenabende/tomcat/jsp/showhdrs.html</a>

</body>
</html>
