<%@ include file="common/getProperties.jsp" 
%><%@ include file="common/header.jsp"
%><%@ include file="common/dbheader.jsp"%><%
        response.setContentType("text/plain");

	String q1 = "select 'users', regexp_replace(session_server, '\\-.*', ''), 'placeholder', count(distinct(session_user)) as num_users, count(session_id) as num_sessions from sakai_session where session_start >= SYSDATE - 1 and session_start = session_end group by regexp_replace(session_server, '\\-.*', '')";

	String sesslen = "select 'sessionlen', '" + dbname + ".' || regexp_replace(session_server, '\\-.*', '') as server, 'placeholder', count(*) as logouts, cast((sum(session_end - session_start) * 86400) as integer) as totsesstime from  sakai_session where session_end > (sys_extract_utc(localtimestamp) - interval '0:05' hour to minute) and session_end <= sys_extract_utc(localtimestamp) and session_end > session_start group by regexp_replace(session_server, '\\-.*', '')";

	String q2 = "select 'event', case when event is null then 'null.event' else event end as event, 'placeholder', count(*) as count, 0 from sakai_event where event_date > (sys_extract_utc(localtimestamp) - interval '0:05' hour to minute) and event_date < sys_extract_utc(localtimestamp) group by event";

	String q3 = "select 'sakai_site', case when type is not null then type else 'null' end as type, 'placeholder', count (title) as count, 0 from sakai_site group by type";

	String q4 = "select 'sakai_realm', substr(provider_id, 0, 6), 'placeholder', count(*),0 from sakai_realm where provider_id like '____,_,%' group by substr(provider_id, 0, 6)";

	String q5 = "select 'sakai_site_tool', translate(registration, '-', '.'), 'placeholder', count(*), 0 from sakai_site_tool group by registration";

	// need to translate "-" into something else otherwise it eats all the subsequent output (jsp/http only?)
	String q6 = "select 'affiliate_usage', case when a.user_affiliation is null then 'unknown affiliation' else translate(a.user_affiliation, '-', '.') end, case when e.event is null then 'null' else e.event end, count(*), 0 from sakai_user_id_map i left outer join db_monitor.user_affiliation a on i.eid = a.user_id, sakai_session s, sakai_event e where s.session_user = i.user_id and s.session_id = e.session_id and e.event_date > (sys_extract_utc(localtimestamp) - interval '0:05' hour to minute) and e.event_date < sys_extract_utc(localtimestamp) group by a.user_affiliation, e.event";

	String q7 = "select 'affiliate_usage', 'friend', e.event, count(*) from sakai_user_id_map i right outer join sakai_session s  on s.session_user = i.user_id, sakai_event e where e.event_date > (sys_extract_utc(localtimestamp) - interval '0:05' hour to minute) and e.event_date < sys_extract_utc(localtimestamp) and i.eid like '%@%' group by a.user_affiliation, e.event";

	String q8 = "select 'dept_usage', case when a.career is null then 'unknown dept' else translate(a.career, '-', '.') end, case when e.event is null then 'null' else e.event end, count(*), 0 from sakai_user_id_map i left outer join db_monitor.user_dept a on i.eid = a.user_id, sakai_session s, sakai_event e where s.session_user = i.user_id and s.session_id = e.session_id and e.event_date > (sys_extract_utc(localtimestamp) - interval '0:05' hour to minute) and e.event_date < sys_extract_utc(localtimestamp) group by a.career, e.event";

	Vector rsets = new Vector();
	rsets.addElement(q1);
	rsets.addElement(q2);
	rsets.addElement(q3);

	//access to sakai_realm has been turned off for ctools_readonly as of 2010-01-21 11:00EDT by qszhu
	// turned back on as of 2010-01-25 10:13EDT as per qszhu rebuilding indices without parallel
	rsets.addElement(q4);

	rsets.addElement(q5);
	rsets.addElement(sesslen);	
	
	if (dbname.equals("CTOOLS")){
		//this is too expensive to run in production
		//rsets.addElement(q7);
		rsets.addElement(q8);
	} else {
	}

        Statement stmt = conn.createStatement();

	for (Enumeration e = rsets.elements(); e.hasMoreElements();){
		String qr = (String) e.nextElement();
		try {
		        ResultSet rset = stmt.executeQuery(qr);
			ResultSetMetaData rsmd = rset.getMetaData();
		     	int numberOfColumns = rsmd.getColumnCount();
		        while (rset.next()){
				for ( int i = 1; i <= numberOfColumns; i++){ 
 		                	String val = (rset.getString(i)).replaceAll("-.*", "");
%><%=val%>|<%
				}
%>
<%
		        }
			rset.close();
		} catch (Exception ex){
			%>ERROR|<%=qr%>|<%=ex.toString()%><%		
		}
	}
        stmt.close();

%><%@ include file="common/dbfooter.jsp"
%><%@ include file="common/footer.jsp" %>
