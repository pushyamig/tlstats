<%@ page language="java" import="java.util.*,java.sql.*,oracle.jdbc.driver.*,javax.naming.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%>
<%!
private boolean isMember(String grp, String un) throws Exception {	
	boolean isMem = false;
	Hashtable env = new Hashtable();
	env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
	env.put(Context.PROVIDER_URL, "ldap://ldap.itd.umich.edu:389/dc=umich,dc=edu");

	try {
	    javax.naming.directory.DirContext ctx = new javax.naming.directory.InitialDirContext(env);
	    String[] attrIDs = {"member"};

	    javax.naming.directory.SearchControls ctls = new javax.naming.directory.SearchControls();
	    String filter = "(&(cn=" + grp + ") (objectclass=rfc822MailGroup))";
	    ctls.setReturningAttributes(attrIDs);
	    ctls.setReturningObjFlag(true);
	    ctls.setSearchScope(javax.naming.directory.SearchControls.SUBTREE_SCOPE);
	    String searchBase = "ou=Groups";
	    javax.naming.NamingEnumeration s = ctx.search(searchBase, filter, ctls);
	    String positiveMatch = "uid=" + un + ",";
	    while (s.hasMore()) {
		javax.naming.directory.SearchResult sr = (javax.naming.directory.SearchResult)s.next();
		javax.naming.NamingEnumeration e = (sr.getAttributes()).getAll();
		while (e.hasMoreElements()){
			javax.naming.directory.Attribute attr = (javax.naming.directory.Attribute) e.nextElement();
			javax.naming.NamingEnumeration a = attr.getAll();
			while (a.hasMoreElements()){
				String val = (String) a.nextElement();
				if(val.indexOf(positiveMatch) != -1){
					isMem = true;
				}
			}
			a.close();
		}
		e.close();
	    }
	    s.close();
	    ctx.close();
	    return isMem;
	} catch (javax.naming.NamingException e) {
	    System.err.println("Problem getting attribute:" + e);
	    return false;
	}
}
%>
